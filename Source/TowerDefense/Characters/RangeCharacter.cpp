// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Objects/SimpleProjectile.h"
#include "RangeCharacter.h"

ARangeCharacter::ARangeCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Status
	baseSpeed = 300;
	maxLife = 100.0;
	life = 100;
	attackRate = 2.5;
	accumulatedTime = 0;
	damage = 25;

	//Carrega o blueprint do tipo de projetil usado
	//**SIMPLE PROJECTILE **//
	static ConstructorHelpers::FObjectFinder<UClass>
		ProjectileObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/Objects/BP_SimpleProjectile.BP_SimpleProjectile_C'"));
	if (ProjectileObj.Object)
	{
		BP_Projectile = (UClass*)ProjectileObj.Object;
	}
}

// Called every frame
void ARangeCharacter::Tick(float DeltaTime)
{
	//Verifica se tem algum alvo. Se n�o tem escolhe um
	if (currTarget == NULL)
	{
		currTarget = SelectTarget();
	}
	//Se j� tem um alvo ent�o incrementa o tempo acumulado
	else
	{
		accumulatedTime += DeltaTime;
	}

	//Se j� passou o tempo attackRate desde o �ltimo attack e tem algum algo ent�o ataca
	if (accumulatedTime > 1 / attackRate && currTarget != NULL)
	{
		accumulatedTime = 0;
		Attack();
	}

	Super::Tick(DeltaTime);
}
void ARangeCharacter::OnEnterInRange(AStaticEnemy* target)
{
	targetsCollection.Add(target);
}

void ARangeCharacter::OnExitRange(AStaticEnemy* target)
{
	int itemPos;
	if (targetsCollection.Find(target, itemPos))
	{
		targetsCollection.RemoveAt(itemPos, 1, true);
		//Se o alvo saiu da area de ataque ent�o perdeu o currTarget
		if (currTarget == target) {
			currTarget = NULL;
		}
	}
}

AStaticEnemy * ARangeCharacter::SelectTarget()
{
	if (targetsCollection.Num() > 0)
	{
		return currTarget = targetsCollection.HeapTop();
	}
	return NULL;
}

void ARangeCharacter::Attack()
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		//Parametros para spawn
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		//O projetil deve ser criado no topo da torre.
		FVector actorLoc = GetActorLocation();

		float collisionRadius;
		float halfHeight;
		GetCapsuleComponent()->CalcBoundingCylinder(collisionRadius, halfHeight);

		float projectileZLoc = actorLoc.Z + halfHeight;
		//Tiramos um quinto do tamanho da torre para garantir que fica dentro da torre
		projectileZLoc = projectileZLoc - halfHeight / 5;

		FVector projectileLoc = FVector(actorLoc.X, actorLoc.Y, projectileZLoc);


		ASimpleProjectile* const projectile = world->SpawnActor<ASimpleProjectile>(BP_Projectile,
			projectileLoc, this->GetActorRotation(), spawnParams);
		//Seta o alvo para o projetil
		projectile->Start(currTarget, damage);
	}

}

void ARangeCharacter::SetHeroStatus()
{
	Super::SetHeroStatus();

	//Editar status do minion aqui
	baseSpeed = 400;
	maxLife = 100.0;
	life = 100;
	attackRate = 1.5;
	accumulatedTime = 0;
	damage = 50;
}
