// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Characters/WalkCharacter.h"
#include "DefensorCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ADefensorCharacter : public AWalkCharacter
{
	GENERATED_BODY()
	
public:
	// Sets default values for this character's properties
	ADefensorCharacter();

	virtual void NotifyIsUnderAttack(bool isUnderAttack) override;

	//Consede ao character status de hero 
	virtual void SetHeroStatus() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
		float workSpeedDrop;

private:

	bool isDefending;

};
