// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../MapComponents/PathPoint.h"
#include "../Characters/DefensorCharacter.h"
#include "../Characters/RangeCharacter.h"
#include "../Characters/PawnCharacter.h"
#include "../Characters/SpyCharacter.h"
#include "../Characters/WalkCharacter.h"
#include "../GameState/MyGameState.h"
#include "GameFramework/Actor.h"
#include "MinionsSpawner.generated.h"

UCLASS()
class TOWERDEFENSE_API AMinionsSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMinionsSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Spline component que o minions instanciado ir� seguir
	UPROPERTY(EditAnywhere, Category = "Must Set")
	APathPoint* spline;
	
	//M�todo chamado para instanciar um objecto da classe passada
	UFUNCTION(BlueprintCallable, Category = "Spawn")
	void SpawnMinion(ECharactersType type, bool hasHeroStatus);


	//**REMOVER APOS AJUSTE DOS VALORES
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	bool rangeHasChangedValues;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	bool spyHasChangedValues;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	bool defensorHasChangedValues;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	bool pawnHasChangedValues;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float rangeMaxLife;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float rangeSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	int32 rangeDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float rangeAtkRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float spyMaxLife;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float spySpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float persuation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float defensorMaxLife;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float defensorSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float defensorSlow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float pawnMaxLife;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
	float pawnSpeed;
	

private:
	//Blueprints dos tipos de minions
	UClass* BP_defensor;
	UClass* BP_range;
	UClass* BP_spy;
	UClass* BP_pawn;

	UBoxComponent* box;

	//REMOVER APOS TESTES
	void setChangedValues(ECharactersType type, AWalkCharacter* minion);
};
