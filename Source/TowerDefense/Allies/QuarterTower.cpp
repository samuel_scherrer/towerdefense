// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "QuarterTower.h"

AQuarterTower::AQuarterTower()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	isQuarterOpen = false;

	life = 300;
	maxLife = life;
}

void AQuarterTower::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (life <= 0)
	{
		DestroyMinionsInside();
	}
}

//Todos os minions que est�o na listOfCharacters s�o minions que est�o na �rea da torre
//e ainda n�o entraram na torre. Por isso ao abrir a torre vare-se essa lista e manda
//os minions que est�o nela irem para a torre. Quando o minion entra na torre ele � 
//removido da listaOfCharacters e passa para a lista respectiva a sua classe
void AQuarterTower::OpenQuarter()
{
	for (AWalkCharacter *actor : listOfCharacters)
	{
		SetCharacterToQuarter(actor);
	}
	isQuarterOpen = true;
}

void AQuarterTower::CloseQuarter()
{
	isQuarterOpen = false;
	//Informa o novo status para todos os minions pois algum pode estar a caminho
	//da torre
	OnStatusChanged.Broadcast(false, this);
}

//Libera um minion que estava dentro da torre para fora. Esse minion n�o volta
//para a listOfCharacters, pois uma vez que um minion entrou na torre n�o pode voltar mais
void AQuarterTower::ReleaseMinion(ECharactersType type)
{
	//Pega o primeiro da lista, coloca pra andar e torma visivel novamente
	//adiciona novamente na lista dos que est�o dentro do range do quartel
	switch (type)
	{
	case ECharactersType::T_RANGE:
		if (rangesList.Num() > 0)
		{
			ARangeCharacter* range = rangesList.Pop();
			range->SetActorHiddenInGame(false);
			SetCharacterBackToPath(range);
		}
		break;
	case ECharactersType::T_DEFENSOR:
		if (defensorsList.Num() > 0)
		{
			ADefensorCharacter* defensor = defensorsList.Pop();
			defensor->SetActorHiddenInGame(false);
			SetCharacterBackToPath(defensor);
		}
		break;
	case ECharactersType::T_SPY:
		if (spiesList.Num() > 0)
		{
			ASpyCharacter* spy = spiesList.Pop();
			spy->SetActorHiddenInGame(false);
			SetCharacterBackToPath(spy);
		}
		break;
	case ECharactersType::T_PAWN:
		if (pawnsList.Num() > 0)
		{
			APawnCharacter* pawn = pawnsList.Pop();
			pawn->SetActorHiddenInGame(false);
			SetCharacterBackToPath(pawn);
		}
		break;
	default:
		break;
	}
}

//Metodo chamado por delegate OnMoveToFinish, o character informa quando terminou de realizar o MoveTo.
//Esse m�todo pode ser chamado em 2 situa��es: 1- minion chegou na torre e ficou hidden (colocar ele para dentro da torre)
//2- Minion estava indo para a torre por�m essa fechou ent�o ele passou a se mover de volta para sua rota e informa que chegou na rota
//(ignoramos essse caso pois s� removemos os delegates quando o minion sair da �rea de atua��o da torre)
void AQuarterTower::PutInQuarter(AWalkCharacter* target)
{
	//Se o minion completou o movimento e est� em hidden ent�o ele chegou na torre, colocar ele pra dentro
	if (target->GetCharacterState() == ECharacterStates::S_HIDDEN)
	{
		//Chama o super para remover o target da lista de minions na area da torre
		Super::OnExitAreaOfAction(target);

		//Remove o minion da lista de notificados sobre o status da torre. Uma vez que o personagem saiu do quartel n�o precisa
		//mais sobre o status dela
		OnStatusChanged.RemoveDynamic(target, &AWalkCharacter::QuarterNewStatus);

		//Como o minion entrou na torre essa n�o deve mais ser informada sobre ele terminar de realizar o moveTo
		target->OnMoveToFinish.RemoveDynamic(this, &AQuarterTower::PutInQuarter);

		ADefensorCharacter* defensor = Cast<ADefensorCharacter>(target);
		ARangeCharacter* range = Cast<ARangeCharacter>(target);
		APawnCharacter* pawn = Cast<APawnCharacter>(target);
		ASpyCharacter* spy = Cast<ASpyCharacter>(target);
		if (target)
		{
			if (defensor)
			{
				defensorsList.Add(defensor);
			}
			else if (range)
			{
				rangesList.Add(range);
			}
			else if (pawn)
			{
				pawnsList.Add(pawn);
			}
			else if (spy)
			{
				spiesList.Add(spy);
			}

		}
	}
}

//Seta os delegates, se j� n�o tiverem sido setados e manda o character ir em dire��o a torre
void AQuarterTower::SetCharacterToQuarter(AWalkCharacter* character)
{
	if (!character->OnMoveToFinish.IsBound())
	{
		//Passamos a funcao de callback
		character->OnMoveToFinish.AddDynamic(this, &AQuarterTower::PutInQuarter);
	}
	character->SetMoveToLocation(GetActorLocation(), ECharacterStates::S_HIDDEN);
}

//Manda o character ir endire��o a rota que ele deve seguir
void AQuarterTower::SetCharacterBackToPath(AWalkCharacter * character)
{
	//� o ponto onde o minion estava antes de se mover para a torre
	FVector entrancePoint = character->GetPathLocationWithDistance(character->walkedDistance);
	//distacia entre a torre e o ponto onde o minion estava antes de entrar nela
	float distance = (FVector2D(entrancePoint.X, entrancePoint.Y) - FVector2D(GetActorLocation().X, GetActorLocation().Y)).Size();
	character->walkedDistance = character->walkedDistance + distance;
	FVector nextLoc = character->GetPathLocationWithDistance(character->walkedDistance);
	character->SetMoveToLocation(nextLoc, ECharacterStates::S_WALKING);
}

void AQuarterTower::DestroyMinionsInside()
{
	for (ARangeCharacter* range : rangesList)
	{
		if (range)
		{
			range->life = 0;
		}
	}
	for (ASpyCharacter* spy : spiesList)
	{
		if (spy)
		{
			spy->life = 0;
		}
	}
	for (APawnCharacter* pawn : pawnsList)
	{
		if (pawn)
		{
			pawn->life = 0;
		}
	}
	for (ADefensorCharacter* tanker : defensorsList)
	{
		if (tanker)
		{
			tanker->life = 0;
		}
	}
}

void AQuarterTower::OnEnterAreaOfAction(AWalkCharacter * target)
{
	//Se o quartel est� aberto coloca o target pra dentro
	if (isQuarterOpen)
	{
		SetCharacterToQuarter(target);
	}
	//Adiciona na lista de minions que est�o na �rea
	listOfCharacters.Add(target);

	//Seta o delegate para que o character receba a atualizacao dos status da torre
	OnStatusChanged.AddDynamic(target, &AWalkCharacter::QuarterNewStatus);
}

//Ao sair da �rea de atua��o remover os delegates e remover da listOfCharacters se ele ainda estiver nela
void AQuarterTower::OnExitAreaOfAction(AWalkCharacter* target)
{
	//Chama o super para remover o minion da lista de minions na area
	Super::OnExitAreaOfAction(target);
	//Como o minion saiu da area de alcance da torre remove ele da lista de calllbacks sobre o status da torre
	OnStatusChanged.RemoveDynamic(target, &AWalkCharacter::QuarterNewStatus);

	//Como o minion entrou na torre essa n�o deve mais ser informada sobre ele terminar de realizar o moveTo
	target->OnMoveToFinish.RemoveDynamic(this, &AQuarterTower::PutInQuarter);
}

//M�todo usado pela HUD
int32 AQuarterTower::GetTotalOf(ECharactersType type)
{
	switch (type)
	{
	case ECharactersType::T_RANGE:
		return rangesList.Num();
	case ECharactersType::T_DEFENSOR:
		return defensorsList.Num();
	case ECharactersType::T_SPY:
		return spiesList.Num();
	case ECharactersType::T_PAWN:
		return pawnsList.Num();
	default:
		return 0;
	}
}
