// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Blueprint/UserWidget.h"
#include "StaticEnemy.h"


// Sets default values
AStaticEnemy::AStaticEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	isUnderSpyRange = false;
	isMarkedForDestroy = false;

	//Carrega o blueprint da widget de vida
	//**Health Widget **//
	static ConstructorHelpers::FObjectFinder<UClass>
		HealthWidgetObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/HUD/BP_HealthBarWidget.BP_HealthBarWidget_C'"));
	if (HealthWidgetObj.Object)
	{
		BP_HealthWidget = HealthWidgetObj.Object;
	}

	//**Loyalty Widget **//
	static ConstructorHelpers::FObjectFinder<UClass>
		LoyaltyObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Enemies/HUD/BP_LoyaltyBarWidget.BP_LoyaltyBarWidget_C'"));
	if (LoyaltyObj.Object)
	{
		BP_LoyaltyWidget = LoyaltyObj.Object;
	}

	//Cria um root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	//Cria o widget da barra de vida
	healthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthWidget"));
	healthWidget->SetRelativeLocation(FVector(0, 0, 200));
	healthWidget->SetWidgetClass(BP_HealthWidget);
	healthWidget->SetupAttachment(RootComponent);

	//Cria o widget da barra de loyalty
	loyaltyWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("LoyaltyWidget"));
	loyaltyWidget->SetRelativeLocation(FVector(0, 0, 300));
	loyaltyWidget->SetWidgetClass(BP_LoyaltyWidget);
	loyaltyWidget->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AStaticEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaticEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//Informa o widget da vida
	FString command = FString::Printf(TEXT("Update %f"), (life / maxLife));
	FOutputDeviceDebug debug;
	healthWidget->GetUserWidgetObject()->CallFunctionByNameWithArguments(*command, debug, NULL, true);

	//Informa o widget de loyalty
	command = FString::Printf(TEXT("Update %f"), (loyalty / maxLoyalty));
	loyaltyWidget->GetUserWidgetObject()->CallFunctionByNameWithArguments(*command, debug, NULL, true);

	//Se est� sob o alcance de spy
	if (listOfSpies.Num() > 0)
	{
		isUnderSpyRange = true;
		towerMesh->SetRenderCustomDepth(true);
	}
	//Caso n�o esteja sob o alcance de spy
	else
	{
		isUnderSpyRange = false;
		towerMesh->SetRenderCustomDepth(false);
	}

	if (life <= 0)
	{
		NotifyOfDestruction();
		NotifySpiesOfDestruction();
		//Remove targets da lista antes de destruir a torre
		targetsCollection.Empty();
		Destroy();
		return;
	}

	//Aqui nao precisamos notificar os spies da destrui��o da torre pois 
	//foi um proprio spy que causou isso
	if (isMarkedForDestroy)
	{
		Destroy();
		return;
	}

}

void AStaticEnemy::OnCharacterEnterAreaOfAttack(AWalkCharacter* target)
{
	targetsCollection.Add(target);
	CheckCollectionForDefensor();
}

void AStaticEnemy::OnAllyEnterAreaOfAttack(AStaticAlly* target)
{
	alliesCollection.Add(target);
}

void AStaticEnemy::OnCharacterExitAreaOfAttack(AWalkCharacter* target)
{
	int itemPos;
	if (targetsCollection.Find(target, itemPos))
	{
		targetsCollection.RemoveAt(itemPos, 1, true);
		AWalkCharacter* character = Cast<AWalkCharacter>(currTarget);
		//Se o alvo saiu da area de ataque ent�o perdeu o currTarget
		if (character)
		{
			if (character == target) 
			{
				//Informa que n�o � mais um alvo
				character->NotifyIsUnderAttack(false);
				currTarget = NULL;
			}
		}

		//Se � um spy saindo do range remove-lo da lista, pois ele pode estar saindo pois foi morto
		ASpyCharacter* spy = Cast<ASpyCharacter>(target);
		if (spy) 
		{
			RemoveFromSpiesList(spy);
		}

		CheckCollectionForDefensor();
	}
}

void AStaticEnemy::OnAllyExitAreaOfAttack(AStaticAlly* target)
{
	int itemPos;
	if (alliesCollection.Find(target, itemPos))
	{
		alliesCollection.RemoveAt(itemPos, 1, true);
		//Se o alvo saiu da area de ataque ent�o perdeu o currTarget
		if (currTarget == Cast<AActor>(target)) {
			currTarget = NULL;
		}
	}
}

void AStaticEnemy::CheckCollectionForDefensor()
{
	//Limpa a cole��o
	defensorCollection.Empty();

	for (AWalkCharacter* target : targetsCollection)
	{
		//Avalia para cada WalkCharacter no targetsCollection se o target � do tipo defensorCharacter
		ADefensorCharacter* defensor = Cast<ADefensorCharacter>(target);
		if (defensor)
		{
			//Se for deste tipo ent�o armazena na cole��o de defensor
			defensorCollection.Add(defensor);
		}
	}
}

void AStaticEnemy::Attack()
{
}

void AStaticEnemy::NotifySpiesOfDestruction()
{
	for (ASpyCharacter* spy : listOfSpies)
	{
		//Se o spy est� em algum estado de trabalho, manda parar
		if (spy->GetCharacterState() == ECharacterStates::S_WORKING ||
			spy->GetCharacterState() == ECharacterStates::S_STARTWORK)
		{
			//Manda o spy voltar a andar, limpa o target e remove qualquer marca de destroy
			spy->isMarkedToDestroy = false;
			spy->SetCharacterState(ECharacterStates::S_ENDWORK);
		}
	}
}

void AStaticEnemy::NotifyOfDestruction()
{
	for (AWalkCharacter *target : targetsCollection)
	{
		target->NotifyIsUnderAttack(false);
	}
	for (ADefensorCharacter *target : defensorCollection)
	{
		target->NotifyIsUnderAttack(false);
	}
}

AActor* AStaticEnemy::SelectTarget()
{
	return nullptr;
}

void AStaticEnemy::SetTowerMeshExtent(FVector BoxExtent)
{
	towerMeshBoxExtent = BoxExtent;
}

void AStaticEnemy::SetTowerMesh(UStaticMeshComponent* mesh)
{
	towerMesh = mesh;
}

FVector const AStaticEnemy::GetTowerMeshExtent()
{
	return towerMeshBoxExtent;
}

void AStaticEnemy::AddOnSpiesList(ASpyCharacter * spy)
{
	listOfSpies.Add(spy);
}

void AStaticEnemy::RemoveFromSpiesList(ASpyCharacter * spy)
{
	int itemPos;
	if (listOfSpies.Find(spy, itemPos))
	{
		listOfSpies.RemoveAt(itemPos, 1, true);
	}
}

void AStaticEnemy::MarkForDestroy(ASpyCharacter * spy)
{
	if (isMarkedForDestroy == false)
	{
		isMarkedForDestroy = true;
		spy->isMarkedToDestroy = true;
	}
}

void AStaticEnemy::NotifySelectedTower()
{
	if (isUnderSpyRange)
	{
		for (ASpyCharacter* spy : listOfSpies)
		{
			//Procura o primeiro que est� desocupado
			if (spy->GetCharacterState() == ECharacterStates::S_WALKING)
			{
				spy->SetCurrTarget(this);
				spy->SetCharacterState(ECharacterStates::S_STARTWORK);
				break;
			}
		}
	}
}
