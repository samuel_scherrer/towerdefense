// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Utils/GameStateDao.h"
#include "Utils/GameStateVo.h"
#include "MyGameState.h"

AMyGameState::AMyGameState()
{
	savePath = FPaths::GameDir() + "\\mysavefile.save";
	//gameStateDao = NewObject<UGameStateDao>(GetTransientPackage(), UGameStateDao::StaticClass());
	//gameStateVo = NewObject<UGameStateVo>(GetTransientPackage(), UUGameStateVo::StaticClass());
	gameStateDao = new UGameStateDao();
	gameStateVo = new UGameStateVo();
	if (hasSavedGame()) 
	{
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOADSTATE);
		execState();
	}
	else {
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_ONMENU);
	}
}

bool AMyGameState::updateSpiesCount()
{
	if (gameStateVo->totalOfSpies > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn spy init %d"), gameStateVo->totalOfCharactersWalking);
		gameStateVo->updateSpiesCount();
		UE_LOG(LogTemp, Warning, TEXT("Spawn spy end %d"), gameStateVo->totalOfCharactersWalking);
		
		return true;
	}
	else 
	{
		return false;
	}
}

bool AMyGameState::updateDefensorsCount()
{
	if (gameStateVo->totalOfDefensors > 0)
	{
		gameStateVo->updateDefensorsCount();
		return true;
	}
	else 
	{
		return false;
	}
}

bool AMyGameState::updatePawnsCount()
{
	if (gameStateVo->totalOfPawns > 0)
	{
		gameStateVo->updatePawnsCount();
		return true;
	}
	else
	{
		return false;
	}
}

bool AMyGameState::updateRangesCount()
{
	if (gameStateVo->totalOfRanges > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn range init %d"), gameStateVo->totalOfCharactersWalking);
		gameStateVo->updateRangesCount();
		UE_LOG(LogTemp, Warning, TEXT("Spawn range end %d"), gameStateVo->totalOfCharactersWalking);
		return true;
	}
	else
	{
		return false;
	}
}

bool AMyGameState::updateCanUseHero()
{
	return gameStateVo->updateCanUseHero();
}

void AMyGameState::convertTo(ECharactersType type)
{
	if (gameStateVo->totalOfPawns > 0 && type != ECharactersType::T_PAWN)
	{
		switch (type)
		{
		case ECharactersType::T_RANGE:
			gameStateVo->totalOfRanges++;
			break;
		case ECharactersType::T_DEFENSOR:
			gameStateVo->totalOfDefensors++;
			break;
		case ECharactersType::T_SPY:
			gameStateVo->totalOfSpies++;
			break;
		}
		gameStateVo->totalOfPawns--;
	}
}

bool AMyGameState::hasSavedGame()
{
	return FPlatformFileManager::Get().GetPlatformFile().FileExists(*savePath);
}

void AMyGameState::setDifficultLevel(int32 difficult)
{
	gameStateVo->difficultLevel = difficult;
	gameStateVo->currState = getGameStateAsInt(EGameStates::GS_NEWGAME);
	execState();
}

void AMyGameState::setState(EGameStates newState)
{
	gameStateVo->currState = getGameStateAsInt(newState);
	execState();
}

void AMyGameState::notifyMinionDeath()
{
	//Decrementa a quantidade de minions andando
	gameStateVo->totalOfCharactersWalking--;
	UE_LOG(LogTemp, Warning, TEXT("Notify of dead %d"), gameStateVo->totalOfCharactersWalking);
	if (gameStateVo->CheckLoseCondition())
	{
		//Perdeu
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOSE);
		execState();
	}
}

void AMyGameState::execState() 
{
	switch (getGameStateAsEnum(gameStateVo->currState))
	{
	case EGameStates::GS_LOADSTATE:
		gameStateDao->loadState(*gameStateVo);
		break;
	case EGameStates::GS_WIN:
		//Calcula o rating
		gameStateVo->CalculateRating();
		gameStateVo->UpdateCountersAfterMatch();
		gameStateVo->UpdateLevelAndChapter();

		//************ FIM DO JOGO MANDA PRA O MAIN MENU ************//
		if (gameStateVo->CheckEndGameCondition()) {
			//END GAME - por gora manda para o menu
			gameStateVo->currState = getGameStateAsInt(EGameStates::GS_ONMENU);
			openLevel(FName("Level_Menu"));
		}

		//*********************** IF PARA TESTE **********************
		//VAI FICAR REPETINDO 2 FASES
		//************************************************************
		if(gameStateVo->currLevel == 4)
		{
			gameStateVo->currLevel = 1;
		}

		//Salva estado apos ganhar jogo
		gameStateDao->saveState(*gameStateVo);
		openLevel(FName("Level_Menu"));
		break;
	case EGameStates::GS_LOSE:
		//Setamos novamente o estado de derrota, pois esse foi perdido com o load
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOSE);

		openLevel(FName("Level_Menu"));
		break;
	case EGameStates::GS_NEWGAME:
		gameStateVo->SetupNewGame();

		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOADGAME);
		execState();
		break;
	case EGameStates::GS_LOADGAME:
		//Marcamos os estado como no menu para caso o jogo seja fechado e quando aberto novamente v� para o menu
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_ONMENU);
		//Fazemos um backup do estado salvo
		gameStateVo->UpdateBackup();

		//Limpa dados para inicio da partida
		gameStateVo->ClearValuesForInitMatch();

		gameStateDao->saveState(*gameStateVo);
		openLevel(FName(*(FString("map_" + FString::FromInt(gameStateVo->currLevel)))));
		break;
	default:
		break;
	}
}

void AMyGameState::updateCountMinionsEnd(ECharactersType type)
{
	UE_LOG(LogTemp, Warning, TEXT("Count end init %d"), gameStateVo->totalOfCharactersWalking);
	gameStateVo->updateCountMinionsEnd(getCharacterTypeAsInt(type));
	UE_LOG(LogTemp, Warning, TEXT("Count end end %d"), gameStateVo->totalOfCharactersWalking);
	if (gameStateVo->CheckWinCondition())
	{
		//Ganhou a fase
		gameStateVo->currState = getGameStateAsInt(EGameStates::GS_WIN);
		execState();
	}
}

void AMyGameState::newGame()
{
	gameStateVo->currState = getGameStateAsInt(EGameStates::GS_NEWGAME);
	execState();
}

void AMyGameState::loadGame()
{
	gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOADGAME);
	execState();
}

void AMyGameState::retryGame()
{
	gameStateVo->currState = getGameStateAsInt(EGameStates::GS_LOADGAME);
	gameStateVo->LoadBackup();
	execState();
}

void AMyGameState::openLevel(FName levelName)
{
	//Carrega novo level
	UGameplayStatics::OpenLevel(GetWorld(), levelName);
}

//Chama somente quando usa os bot�es de quit dentro do jogo
void AMyGameState::quitting()
{
	//Prepara para ir para o menu principal quando come�ar na proxima vez
	gameStateVo->currState = getGameStateAsInt(EGameStates::GS_ONMENU);
	saveState();
}

//***** VER QUEM USA E REMOVER
bool AMyGameState::saveState()
{
	return gameStateDao->saveState(*gameStateVo);
}

int32 AMyGameState::getGameStateAsInt(EGameStates state)
{
	switch (state)
	{
	case EGameStates::GS_WIN:
		return WIN_GS;
	case EGameStates::GS_LOSE:
		return LOSE_GS;
	case EGameStates::GS_LOADGAME:
		return LOADGAME_GS;
	case EGameStates::GS_NEWGAME:
		return NEWGAME_GS;
	case EGameStates::GS_LOADSTATE:
		return LOADSTATE_GS;
	case EGameStates::GS_LOADLEVEL:
		return LOADLEVEL_GS;
	case EGameStates::GS_ONMENU:
		return ONMENU_GS;
	case EGameStates::GS_PLAYING:
		return PLAYING_GS;
	default:
		return -1;
	}
}

EGameStates AMyGameState::getGameStateAsEnum(int32 state)
{
	if (state == WIN_GS)
	{
		return EGameStates::GS_WIN;
	}
	else if (state == LOSE_GS)
	{
		return EGameStates::GS_LOSE;
	}
	else if (state == LOADGAME_GS)
	{
		return EGameStates::GS_LOADGAME;
	}
	else if (state == NEWGAME_GS)
	{
		return EGameStates::GS_NEWGAME;
	}
	else if (state == LOADSTATE_GS)
	{
		return EGameStates::GS_LOADSTATE;
	}
	else if (state == LOADLEVEL_GS)
	{
		return EGameStates::GS_LOADLEVEL;
	}
	else if (state == ONMENU_GS)
	{
		return EGameStates::GS_ONMENU;
	}
	else if (state == PLAYING_GS)
	{
		return EGameStates::GS_PLAYING;
	}
	else {
		return EGameStates::GS_ERROR;
	}
}

int32 AMyGameState::getCharacterTypeAsInt(ECharactersType type)
{
	switch (type)
	{
	case ECharactersType::T_RANGE:
		return gameStateVo->RANGE_TYPE;
	case ECharactersType::T_DEFENSOR:
		return gameStateVo->DEFENSOR_TYPE;
	case ECharactersType::T_SPY:
		return gameStateVo->SPY_TYPE;
	case ECharactersType::T_PAWN:
		return gameStateVo->PAWN_TYPE;
	case ECharactersType::T_HERO:
		return gameStateVo->HERO_TYPE;
	default:
		return -1;
	}
}

ECharactersType AMyGameState::getCharacterTypeAsEnum(int32 type)
{
	if (type == gameStateVo->RANGE_TYPE)
	{
		return ECharactersType::T_RANGE;
	}
	else if (type == gameStateVo->DEFENSOR_TYPE)
	{
		return ECharactersType::T_DEFENSOR;
	}
	else if (type == gameStateVo->SPY_TYPE)
	{
		return ECharactersType::T_SPY;
	}
	else if (type == gameStateVo->PAWN_TYPE)
	{
		return ECharactersType::T_PAWN;
	}
	else if (type == gameStateVo->HERO_TYPE)
	{
		return ECharactersType::T_HERO;
	}
	else
	{
		return ECharactersType::T_ERROR;
	}
}