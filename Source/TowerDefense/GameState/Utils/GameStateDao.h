// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameStateVo.h"

class UGameStateDao {

public:
	UGameStateDao();

	bool loadState(UGameStateVo& gameStateVo);
	bool saveState(UGameStateVo& gameStateVo);

private:
	void saveData(FArchive& ar, UGameStateVo& gameStateVo);
	void loadData(FArchive& ar, UGameStateVo& gameStateVo);

	FString savePath;
};