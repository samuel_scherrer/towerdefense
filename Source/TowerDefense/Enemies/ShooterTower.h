// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enemies/StaticEnemy.h"
#include "ShooterTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AShooterTower : public AStaticEnemy
{
	GENERATED_BODY()

public:
	AShooterTower();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, Category = "Must Set")
		TSubclassOf<class AProjectile> projectileToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float damage;
	
protected:
	virtual AActor* SelectTarget() override;
	
	virtual void Attack() override;


private: 
	void CheckForDefensorCharacter();
};
