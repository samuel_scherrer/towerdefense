// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Allies/StaticAlly.h"
#include "HealingTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AHealingTower : public AStaticAlly
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHealingTower();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AllyProperties")
	float healAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AllyProperties")
	float actionRate;
private:
	float accumulatedTime;
	
	void Heal();
};
