// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../../Enemies/StaticEnemy.h"
#include "GameFramework/Actor.h"
#include "SimpleProjectile.generated.h"

UCLASS()
class TOWERDEFENSE_API ASimpleProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASimpleProjectile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void Start(AStaticEnemy* actor, int projDmg);

	UFUNCTION(BlueprintCallable, Category = "Hit")
		void OnHit(AActor* reachedTarget);
	
private:
	//Define a velocidade do projetil
	float velocity;
	float damage;
	FVector direction;

	AStaticEnemy* target;

	void Move(float DeltaTime);
};
