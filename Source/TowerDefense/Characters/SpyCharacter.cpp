// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "SpyCharacter.h"
#include "../Enemies/StaticEnemy.h"
#include "../Objects/AlliesSpawner.h"

ASpyCharacter::ASpyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Propriedades do personagem
	maxLife = 300.0;
	life = 300.0;
	baseSpeed = 120;
	persuasion = 0.1;
	isMarkedToDestroy = false;

	//Inicia sem alvos
	currTarget = NULL;

	//Carregar o BP do component de selecao de torre aliada
	static ConstructorHelpers::FObjectFinder<UClass>
		AllySpawnerObj(TEXT("Class'/Game/TopDownCPP/Blueprints/MapComponents/BP_AllyTowerSpawner.BP_AllyTowerSpawner_C'"));
	if (AllySpawnerObj.Object)
	{
		BP_AllyTowerSpawner = AllySpawnerObj.Object;
	}
}

// Called every frame
void ASpyCharacter::Tick(float DeltaTime)
{
	if (!wasDestroyCalled) {
		ExecCharacterState(DeltaTime);
	}
	Super::Tick(DeltaTime);
}

void ASpyCharacter::ExecCharacterState(float DeltaTime)
{
	FVector direction;
	switch (characterState)
	{
	case ECharacterStates::S_STARTWORK:
		//Guardamos a posicao da torre alvo
		towerLoc = currTarget->GetActorLocation();

		//Rotacionamos o spy na dire��o da torre
		direction = towerLoc - GetActorLocation();
		SetActorRotation(direction.Rotation());

		//Mudamos o estado
		characterState = ECharacterStates::S_WORKING;
		break;
	case ECharacterStates::S_WORKING:
		Spy();
		//Se dominou a torre
		if (currTarget->loyalty <= 0)
		{
			//Informa a torre que ele � um dos possiveis spies a ser destruido
			currTarget->MarkForDestroy(this);
			characterState = ECharacterStates::S_ENDWORK;
		}
		break;
	case ECharacterStates::S_ENDWORK:
		//Se esse � o spy marcada para ser destruido 
		if (isMarkedToDestroy)
		{
			//guarda a posi��o em que a torre est�
			towerLoc = currTarget->GetActorLocation();
			//Spawn da caixa de tower amiga
			SpawnAllyTowerSelector(towerLoc);
			//Marca como morto. //Destroi esse spy pois ele foi gasto dominando a torre
			characterState = ECharacterStates::S_DEAD;
		}
		//Se nao volta a andar normalmente
		else
		{
			currTarget = NULL;
			characterState = ECharacterStates::S_WALKING;
		}
		break;
	default:
		break;
	}

	AWalkCharacter::ExecCharacterState(DeltaTime);
}

void ASpyCharacter::OnEnterInRange(AStaticEnemy* target)
{
	//Adiciona na sua lista de torres alvos
	targetsCollection.Add(target);
	//Avisa a torre que ela � um alvo dessa classe
	target->AddOnSpiesList(this);
}

void ASpyCharacter::OnExitRange(AStaticEnemy* target)
{
	int itemPos;
	if (targetsCollection.Find(target, itemPos))
	{
		//Remove a torre da lista de torres
		targetsCollection.RemoveAt(itemPos, 1, true);
		//Avisa a torre que ela n�o � mais um alvo deste spy
		target->RemoveFromSpiesList(this);
	}
}

/*AStaticEnemy* ASpyCharacter::CheckForAttackCommand()
{
	for (AStaticEnemy* target : targetsCollection)
	{
		//Se tem algum alvo marcado para ser atacado ent�o pega ele como alvo
		if (target->isSelectedTargetForSpy)
		{
			//Informa que o spy ir� atender ao pedido de ataque
			target->isSelectedTargetForSpy = false;
			return target;
		}
	}
	return NULL;
}*/

void ASpyCharacter::Spy()
{
	//Decrementa a lealdade da torre
	currTarget->loyalty = currTarget->loyalty - persuasion;
}

void ASpyCharacter::SpawnAllyTowerSelector(FVector location)
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		//Parametros para spawn
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		AAlliesSpawner* const allyTowerSelector = world->SpawnActor<AAlliesSpawner>(
			BP_AllyTowerSpawner, location, FRotator(0,0,0), spawnParams);
	}
}


void ASpyCharacter::SetHeroStatus()
{
	Super::SetHeroStatus();

	//Editar status do minion aqui
	maxLife = 500.0;
	life = 500.0;
	baseSpeed = 220;
	persuasion = 0.5;
}