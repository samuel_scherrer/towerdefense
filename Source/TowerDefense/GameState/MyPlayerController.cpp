// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "MyPlayerController.h"
#include "../Enemies/StaticEnemy.h"
#include "../Objects/AlliesSpawner.h"
#include "CameraPawn.h"

AMyPlayerController::AMyPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Hand;
}

void AMyPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if (bMouseBtnClicked)
	{
		//Atendemos um click por vez
		bMouseBtnClicked = false;
		CallAction();
	}

	if (bIsFirstTouch && !bIsSecondTouch) {
		CheckForTouchMoves();
	} else if (bIsSecondTouch && bIsFirstTouch) {
		CheckForPinchDistance();
	}
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	//Os nomes usados para os eventos s�o os mesmos que foram setados
	//la no project settings na aba de input
	//Eventos para o click
	InputComponent->BindAction("SetAction", IE_Pressed, this, &AMyPlayerController::OnSetActionPressed);

	//Eventos para touch
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyPlayerController::OnSetActionTouched);

	InputComponent->BindAxis("MoveForward", this, &AMyPlayerController::OnMoveForwardAxis);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayerController::OnMoveRightAxis);
	InputComponent->BindAction("ZoomIn", EInputEvent::IE_Pressed, this, &AMyPlayerController::OnZoomInAction);
	InputComponent->BindAction("ZoomOut", EInputEvent::IE_Pressed, this, &AMyPlayerController::OnZoomOutAction);

	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyPlayerController::OnTouchAction);
	InputComponent->BindTouch(EInputEvent::IE_Released, this, &AMyPlayerController::OnTouchRelease);
}

void AMyPlayerController::OnSetActionPressed()
{
	bMouseBtnClicked = true;
}

void AMyPlayerController::OnTouchAction(const ETouchIndex::Type FingerIndex, const FVector Location) {
	if (FingerIndex == ETouchIndex::Touch1) {
		bIsFirstTouch = true;
		firstTouchInitLoc = FVector2D(Location.X, Location.Y);
	}
	else if (FingerIndex == ETouchIndex::Touch2) {
		bIsSecondTouch = true;
		secondTouchInitLoc = FVector2D(Location.X, Location.Y);
	}
}
void AMyPlayerController::OnTouchRelease(const ETouchIndex::Type FingerIndex, const FVector Location) {
	if (FingerIndex == ETouchIndex::Touch1 && !bIsSecondTouch) {
		bIsFirstTouch = false;
		//Vamos ignorar o eixo z
		FVector2D currLoc = FVector2D(Location.X, Location.Y);
		if (firstTouchInitLoc.Equals(currLoc, 0.5)) {
			OnSetActionTouched(Location);
		}
	}
	else if (FingerIndex == ETouchIndex::Touch2) {
		bIsSecondTouch = false;
	}
}

void AMyPlayerController::CheckForTouchMoves() {
	float currLocX = 0;
	float currLocY = 0;
	bool stillPressed;
	GetInputTouchState(ETouchIndex::Touch1, currLocX, currLocY, stillPressed);

	//Calculamos o vetor de posicao atual, zuando a posicao inicial z pois vamos ignorar o eixo z
	FVector2D currLoc = FVector2D(currLocX, currLocY);

	FVector2D dif = (currLoc - firstTouchInitLoc);

	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		//Somente haver� movimento caso a diferen�a entre a posi��o atual � a posi��o inicial for maior que o threshold
		if (abs((long)dif.Y) > swipeThreshold) {
			cameraPawn->MoveCharacterForward(dif.Y * swipeSpeed);
		}
		if (abs((long)dif.X) > swipeThreshold) {
			//multiplicamos por -1 para colocar no sentido desejado o movimento
			cameraPawn->MoveCharacterRight((-1 * dif.X) * swipeSpeed);
		}
	}

}

void AMyPlayerController::CheckForPinchDistance() {
	float firstTouchCurrLocX = 0;
	float firstTouchCurrLocY = 0;
	bool stillPressed;
	GetInputTouchState(ETouchIndex::Touch1, firstTouchCurrLocX, firstTouchCurrLocY, stillPressed);

	float secondTouchCurrLocX = 0;
	float secondTouchCurrLocY = 0;
	GetInputTouchState(ETouchIndex::Touch2, secondTouchCurrLocX, secondTouchCurrLocY, stillPressed);

	FVector2D firstTouchCurrLoc = FVector2D(firstTouchCurrLocX, firstTouchCurrLocY);
	FVector2D secondTouchCurrLoc = FVector2D(secondTouchCurrLocX, secondTouchCurrLocY);

	float initDistance = (firstTouchInitLoc - secondTouchInitLoc).Size();
	float currDistance = (firstTouchCurrLoc - secondTouchCurrLoc).Size();

	if (currDistance != initDistance) {
		APawn* const Pawn = GetPawn();
		ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
		if (cameraPawn)
		{

			if (currDistance > initDistance) {
				float zoomOutAmount = initDistance - currDistance;
				cameraPawn->ChangeCameraArmLength(zoomOutAmount/3);
			}
			else {
				float zoomInAmount = initDistance - currDistance;
				cameraPawn->ChangeCameraArmLength(zoomInAmount/3);
			}
		}
	}

	firstTouchInitLoc = firstTouchCurrLoc;
	secondTouchInitLoc = secondTouchCurrLoc;
}

//Usando para touch
void AMyPlayerController::OnSetActionTouched(const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult hitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, hitResult);
	if (hitResult.bBlockingHit)
	{
		AStaticEnemy* staticEnemy = Cast<AStaticEnemy>(hitResult.GetActor());
		AAlliesSpawner* alliesSpawner = Cast<AAlliesSpawner>(hitResult.GetActor());
		AQuarterTower* quarterTower = Cast<AQuarterTower>(hitResult.GetActor());
		if (staticEnemy)
		{
			staticEnemy->NotifySelectedTower();
		}
		else if (alliesSpawner)
		{
			GetMyHUD()->SetAlliesSpawner(alliesSpawner);

			//Move a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(alliesSpawner->GetActorLocation());
			}
		}
		else if (quarterTower)
		{
			GetMyHUD()->SetQuarterTower(quarterTower);

			//Vemo a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(quarterTower->GetActorLocation());
			}
		}
	}
}

void AMyPlayerController::CallAction()
{
	//Trace para ver o que est� sob o mouse
	FHitResult hitResult;
	GetHitResultUnderCursor(ECC_Visibility, false, hitResult);

	if (hitResult.bBlockingHit)
	{
		AStaticEnemy* staticEnemy = Cast<AStaticEnemy>(hitResult.GetActor());
		AAlliesSpawner* alliesSpawner = Cast<AAlliesSpawner>(hitResult.GetActor());
		AQuarterTower* quarterTower = Cast<AQuarterTower>(hitResult.GetActor());
		if (staticEnemy)
		{
			staticEnemy->NotifySelectedTower();
		}
		else if (alliesSpawner)
		{
			GetMyHUD()->SetAlliesSpawner(alliesSpawner);

			//Move a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(alliesSpawner->GetActorLocation());
			}
		}
		else if (quarterTower)
		{
			GetMyHUD()->SetQuarterTower(quarterTower);

			//Vemo a camera para a posi��o do ally
			APawn* const Pawn = GetPawn();
			ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
			if (cameraPawn)
			{
				cameraPawn->MoveToLoc(quarterTower->GetActorLocation());
			}
		}
	}
}

void AMyPlayerController::OnMoveForwardAxis(float axisValue)
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->MoveCharacterForward(axisValue);
	}
}

void AMyPlayerController::OnMoveRightAxis(float axisValue)
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->MoveCharacterRight(axisValue);
	}
}

void AMyPlayerController::OnZoomInAction()
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->ChangeCameraArmLength(-1.0f);
	}
}

void AMyPlayerController::OnZoomOutAction()
{
	APawn* const Pawn = GetPawn();
	ACameraPawn* cameraPawn = Cast<ACameraPawn>(Pawn);
	if (cameraPawn)
	{
		cameraPawn->ChangeCameraArmLength(1.0f);
	}
}

AGameHUD * AMyPlayerController::GetMyHUD()
{
	//Na primeira vez que passa por aqui pode ser null entao recupera a HUD
	//Nao pode colocar no construtor pois a HUD nao ainda n�o ter sido instanciada
	if (myHUD == NULL)
	{
		myHUD = Cast<AGameHUD>(GetHUD());
	}
	return myHUD;
}
