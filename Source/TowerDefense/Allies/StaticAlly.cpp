// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Blueprint/UserWidget.h"
#include "StaticAlly.h"


// Sets default values
AStaticAlly::AStaticAlly()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	//Carrega o blueprint da widget de vida
	//**Health Widget **//
	static ConstructorHelpers::FObjectFinder<UClass>
		HealthWidgetObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/HUD/BP_HealthBarWidget.BP_HealthBarWidget_C'"));
	if (HealthWidgetObj.Object)
	{
		BP_HealthWidget = HealthWidgetObj.Object;
	}

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	healthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthWidget"));
	healthWidget->SetRelativeLocation(FVector(0, 0, 200));
	healthWidget->SetWidgetClass(BP_HealthWidget);
	healthWidget->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AStaticAlly::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaticAlly::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//Informa o widget da vida
	FString command = FString::Printf(TEXT("Update %f"), (life / maxLife));
	FOutputDeviceDebug debug;
	healthWidget->GetUserWidgetObject()->CallFunctionByNameWithArguments(*command, debug, NULL, true);

	if (life <= 0)
	{
		//Remove os characters da lista antes de destruir a torre
		listOfCharacters.Empty();
		Destroy();
	}
}

void AStaticAlly::OnEnterAreaOfAction(AWalkCharacter * target)
{
	listOfCharacters.Add(target);
}

void AStaticAlly::OnExitAreaOfAction(AWalkCharacter * target)
{
	int itemPos;
	if (listOfCharacters.Find(target, itemPos))
	{
		listOfCharacters.RemoveAt(itemPos, 1, true);
	}
}

AActor * AStaticAlly::SelectTarget()
{
	return nullptr;
}

void AStaticAlly::Action()
{

}
