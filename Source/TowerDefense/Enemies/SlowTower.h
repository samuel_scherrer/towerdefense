// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enemies/StaticEnemy.h"
#include "SlowTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASlowTower : public AStaticEnemy
{
	GENERATED_BODY()
	
public:
	ASlowTower();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Events")
	virtual void OnCharacterEnterAreaOfAttack(AWalkCharacter* target) override;

	UFUNCTION(BlueprintCallable, Category = "Events")
	virtual void OnCharacterExitAreaOfAttack(AWalkCharacter* target) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float slowDebuff;

private:
	//Remove o efeito de lentidao antes que a torre suma
	virtual void RemoveEffect();
};
