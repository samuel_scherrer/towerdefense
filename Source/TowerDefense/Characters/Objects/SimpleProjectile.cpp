// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "../../Allies/AttackTower.h"
#include "../../Allies/StaticAlly.h" 
#include "SimpleProjectile.h"


// Sets default values
ASimpleProjectile::ASimpleProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Constantes
	velocity = 5000;
	damage = 25;
}

// Called when the game starts or when spawned
void ASimpleProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASimpleProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	Move(DeltaTime);

}

void ASimpleProjectile::Start(AStaticEnemy* actor, int projDmg)
{
	damage = projDmg;

	target = actor;

	FVector actorLocation = actor->GetActorLocation();
	FVector myLocation = this->GetActorLocation();

	//Ajustamos a componente z para que o projetil acerte na parte superior do actor
	//O projetil deve se dirigir para os membros superiores do alvo. Tiramos um para 
	//garantir que n�o passe sobre a cabe�a
	float targetHeightCorrection = actorLocation.Z + actor->GetTowerMeshExtent().Z / 5;
	FVector predictedLocation = FVector(actorLocation.X, actorLocation.Y, targetHeightCorrection);

	//Calculamos finalmente a dire��o do movimento
	direction = predictedLocation - myLocation;
	direction.Normalize();
}

void ASimpleProjectile::Move(float DeltaTime)
{
	FVector myLoc = this->GetActorLocation();
	myLoc += direction * DeltaTime * velocity;

	this->SetActorRelativeLocation(myLoc);
}

void ASimpleProjectile::OnHit(AActor* reachedTarget)
{
	//Verifica se colidiu com um personagem
	AStaticEnemy* targetEnemy = Cast<AStaticEnemy>(reachedTarget);
	if (targetEnemy)
	{
		if (targetEnemy == target)
		{
			//Se sim ent�o aplica o dano
			targetEnemy->life = targetEnemy->life - damage;

			//Destroy o projetil
			Destroy();
		}
	}
	else if (Cast<AWalkCharacter>(reachedTarget) == nullptr &&
		Cast<AStaticAlly>(reachedTarget) == nullptr)
	{
		//Se n�o � enemy e n�o � minion e n�o � torre alida ent�o bateu em outra coisa destroy
		Destroy();
	}
}
