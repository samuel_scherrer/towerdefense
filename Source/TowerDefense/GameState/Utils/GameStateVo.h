// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UGameStateVo {
	
public:
	UGameStateVo();

	/** Para eliminar a dependencia dessa classe com a unreal usamos 
	/ os enum definidos no jogo como int32 aqui. Isso facilita tanto na 
	/ hora de salvar em arquivo como tamb�m permite essa classe n�o ter dependencias e ser static
	/ No geral temos
	/ Pawn = 0
	/ Range = 1
	/ Defensor = 2
	/ Spy = 3
	*/
	int32 currState;
	int32 totalOfMinionsAvaiableOnMatch;
	int32 totalOfSpies;
	int32 totalOfDefensors;
	int32 totalOfPawns;
	int32 totalOfRanges;
	int32 totalMinionsEnd;
	int32 totalOfSpiesEnd;
	int32 totalOfPawnsEnd;
	int32 totalOfRangesEnd;
	int32 totalOfDefensorsEnd;
	int32 totalOfCharactersWalking;
	float matchRating;
	int32 totalOfMinionsOnBegin;

	int32 backupTotalOfSpies;
	int32 backupTotalOfDefensors;
	int32 backupTotalOfPawns;
	int32 backupTotalOfRanges;
	int32 backupTotalOfMinionsOnBegin;
	int32 backupCurrLevel;
	int32 backupCurrChapter;
	int32 backupHeroType;
	bool backupCanUseHero;

	//Difficult easy = 0; normal = 1; hard = 2
	int32 difficultLevel;

	int32 currLevel;
	int32 currChapter;
	int32 totalOfLevels = 3;
	int32 totalOfChapters = 4;

	bool canUseHero;
	int32 heroType;

	//Gerencia o backup de vari�veis
	void UpdateBackup();
	void LoadBackup();

	void UpdateCountersAfterMatch();
	void CalculateRating();
	void UpdateLevelAndChapter();
	void SetupNewGame();
	void ClearValuesForInitMatch();

	//Conditions
	bool CheckWinCondition();
	bool CheckLoseCondition();
	bool CheckEndGameCondition();

	//Updates
	bool updateCanUseHero();
	void updateRangesCount();
	void updatePawnsCount();
	void updateDefensorsCount();
	void updateSpiesCount();
	void updateCountMinionsEnd(int32 type);

	//Constantes de convers�o de enums
	const int32 PAWN_TYPE = 0;
	const int32 RANGE_TYPE = 1;
	const int32 DEFENSOR_TYPE = 2;
	const int32 SPY_TYPE = 3;
	const int32 HERO_TYPE = 4;

private:
	void NextChapter();
};
