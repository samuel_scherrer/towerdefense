// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "../Allies/HealingTower.h"
#include "../Allies/QuarterTower.h"
#include "../Allies/AttackTower.h" 
#include "AlliesSpawner.h"


// Sets default values
AAlliesSpawner::AAlliesSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Carrega os blueprints de cada tipo de minion
	//**HEALER **//
	static ConstructorHelpers::FObjectFinder<UClass>
		HealingObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Allies/BP_HealingTower.BP_HealingTower_C'"));
	if (HealingObj.Object)
	{
		BP_HealingTower = HealingObj.Object;
	}

	//**QUARTER **//
	static ConstructorHelpers::FObjectFinder<UClass>
		QuarterObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Allies/BP_AllyQuarterTower.BP_AllyQuarterTower_C'"));
	if (QuarterObj.Object)
	{
		BP_QuarterTower = QuarterObj.Object;
	}

	//**ATTACK **//
	static ConstructorHelpers::FObjectFinder<UClass>
		AttackerObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Allies/BP_AttackTower.BP_AttackTower_C'"));
	if (AttackerObj.Object)
	{
		BP_DamagerTower = AttackerObj.Object;
	}
}

void AAlliesSpawner::SpawnAlly(EAlliesType type)
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		//Parametros para spawn
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;
		AStaticAlly *allyTower = NULL;

		//Instancia o personagem dado o parametro recebido
		switch (type)
		{
		case EAlliesType::A_HEALER:
			allyTower = (AStaticAlly*)world->SpawnActor<AHealingTower>(BP_HealingTower,
				this->GetActorLocation(), FRotator(0,0,0), spawnParams);
			break;
		case EAlliesType::A_DAMAGER:
			allyTower = (AStaticAlly*)world->SpawnActor<AAttackTower>(BP_DamagerTower,
				this->GetActorLocation(), FRotator(0, 0, 0), spawnParams);
			break;
		case EAlliesType::A_QUARTER:
			allyTower = (AStaticAlly*)world->SpawnActor<AQuarterTower>(BP_QuarterTower,
				this->GetActorLocation(), FRotator(0, 0, 0), spawnParams);
			break;
		default:
			break;
		}

		//REMOVER APOS OS AJUSTES
		setChangedValues(type, allyTower);
	}
	this->Destroy();
}

void AAlliesSpawner::setChangedValues(EAlliesType type, AStaticAlly* minion)
{
	switch (type)
	{
	case EAlliesType::A_HEALER:
		if (healingTowerChangedValues)
		{
			AHealingTower* healTower = Cast<AHealingTower>(minion);
			healTower->life = healLife;
			healTower->healAmount = healAmount;
			healTower->actionRate = healrate;
		}
		break;
	case EAlliesType::A_DAMAGER:
		if (attackTowerChangedValues)
		{
			AAttackTower* attackTower = Cast<AAttackTower>(minion);
			attackTower->life = attackLife;
			attackTower->damage = attackDamage;
			attackTower->attackRate = attackRate;
		}
		break;
	case EAlliesType::A_QUARTER:
		if (quarterTowerChangedValues)
		{
			AQuarterTower* quarterTower = Cast<AQuarterTower>(minion);
			quarterTower->life = quarterLife;
		}
		break;
	default:
		break;
	}
}

