// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "../MapComponents/PathPoint.h"
#include "Components/WidgetComponent.h"
#include "../GameState/MyGameState.h"
#include "WalkCharacter.generated.h"

class AQuarterTower;

UENUM(BlueprintType)
enum class ECharacterStates : uint8 
{
	S_WALKING		UMETA(DisplayName="S_Walking"),
	S_IDLE			UMETA(DisplayName="S_Idle"),
	S_STARTWORK		UMETA(DisplayName="S_StartWork"),
	S_WORKING		UMETA(DisplayName="S_Working"),
	S_ENDWORK		UMETA(DisplayName="S_EndWork"),
	S_DEAD			UMETA(DisplayName="S_Dead"),
	S_MOVETO		UMETA(DisplayName="S_MoveTo"),
	S_HIDDEN		UMETA(DisplayName="S_Hidden"),
	S_GONE			UMETA(DisplayName="S_Gone")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterMovedToLocation, AWalkCharacter*, target);

UCLASS()
class TOWERDEFENSE_API AWalkCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWalkCharacter();

	//Callback para informar que o minion terminou de andar at� o local
	FCharacterMovedToLocation OnMoveToFinish;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	//Velocidade varia entre 150 e 600 cm/s
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
	float baseSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
	float walkSpeed;
	//Distancia que o personagem andou na spline, valor acumulado
	float walkedDistance = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
	float life;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
	float maxLife;
	bool hasHeroStatus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Life")
	UWidgetComponent* healthWidget;

	UPROPERTY(BlueprintReadOnly, Category = "Life")
	bool lifeChanging = false;

	UPROPERTY(EditAnywhere, Category = "Walk Components")
	APathPoint* pathSplineComponent;
	
	UFUNCTION(BlueprintCallable, Category = "Walk Components")
	void Move(float deltaTime);

	UFUNCTION(BlueprintCallable, Category = "Walk Components")
	float GetWalkSpeed();

	UFUNCTION(BlueprintCallable, Category = "Walk Components")
	void SetWalkSpeed(float newSpeed);

	UFUNCTION(BlueprintCallable, Category = "Character States")
	void SetCharacterState(ECharacterStates state);

	UFUNCTION(BlueprintCallable, Category = "Character States")
	ECharacterStates GetCharacterState() { return characterState; }

	UFUNCTION(BlueprintCallable, Category = "Walk Components")
	void SetSplineComponent(APathPoint* newPath);

	//Seta uma varia��o no x e y com a qual o character vai seguir a spline
	void SetMovementVariation(FVector2D movVariation);

	FVector2D GetMovementVariation();

	//Faz com que o personagem ande em uma determinada situa��o
	void SetMoveToLocation(FVector location, ECharacterStates goToState);

	//Evento chamado pelo seletor de alvos quando o personagem � um alvo ou deixa de ser um
	virtual void NotifyIsUnderAttack(bool isUnderAttack);

	FVector GetPathLocationWithDistance(float distance);

	//Consede ao character status de hero 
	virtual void SetHeroStatus();

	bool GetHeroStatus() { return hasHeroStatus; };

	//Callback que a torre de quartel manda informando seu status
	UFUNCTION()
	void QuarterNewStatus(bool newStatus, AQuarterTower* theQuarter);

protected:
	virtual void ExecCharacterState(float DeltaTime);

	//Flag que indica se ja foi pedido para destruir essa classe
	bool wasDestroyCalled;

	//Armazena o estado atual do personagem
	ECharacterStates characterState;
private:
	//Componente que armazena a varia��o do movimento do personagem em rela��o ao spline
	FVector2D movementVariation;

	//Armazena a dire��o para a qual o personagem deve se mover quando entre no estado MoveTo
	FVector moveToDirection;
	//Indica a distancia ate o local para o qual est� se movendo
	FVector moveToLoc;
	//Estado para o qual o personagem vai apos realizar o moveTo
	ECharacterStates stateAfterMoveTo;

	//Move o personagem na dire��o calculada em SetMoveLocation
	void MoveTo(float deltaTime);

	UClass* BP_HealthWidget;

	AMyGameState* gameState;
};
