// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Characters/WalkCharacter.h"
#include "../Enemies/StaticEnemy.h"
#include "RangeCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ARangeCharacter : public AWalkCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARangeCharacter();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	//Consede ao character status de hero 
	virtual void SetHeroStatus() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
		float attackRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
		int32 damage;

protected:
	UFUNCTION(BlueprintCallable, Category = "Selecting Target")
	void OnEnterInRange(AStaticEnemy* target);

	UFUNCTION(BlueprintCallable, Category = "Selecting Target")
	void OnExitRange(AStaticEnemy* target);

private:
	AStaticEnemy* currTarget;
	float accumulatedTime;

	//Collecao de alvos
	TArray<AStaticEnemy*> targetsCollection;

	//Seleciona o alvo do ataque
	AStaticEnemy* SelectTarget();
	void Attack();

	//Blueprints do projetil
	UClass* BP_Projectile;
};
