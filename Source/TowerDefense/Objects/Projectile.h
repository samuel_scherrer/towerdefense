// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Characters/WalkCharacter.h"
#include "../Allies/StaticAlly.h" 
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UENUM(BlueprintType)
enum class EProjectileStates : uint8
{
	S_MOVING		UMETA(DisplayName = "S_Moving"),
	S_APPLY			UMETA(DisplayName = "S_Apply"),
	S_DURING		UMETA(DisplayName = "S_During"),
	S_FINALIZE		UMETA(DisplayName = "S_Finalize"),
	S_DONE			UMETA(DisplayName = "S_Done")
};

UCLASS()
class TOWERDEFENSE_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	void Start(AActor* target, float dmg);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, Category = "Visual Components")
	UStaticMeshComponent* mesh;

	UFUNCTION(BlueprintCallable, Category = "Hit")
	void OnHit(AActor* reachedTarget);

protected:
	UFUNCTION(BlueprintCallable, Category = "Components")
	void SetStaticMeshComponent(UStaticMeshComponent* staticMesh);

private:
	//Define a velocidade do projetil
	float velocity;
	FVector direction;
	AActor* target;
	
	//Armazena o estado do projetil
	EProjectileStates projectileState;

	float damage;

	void Move(float DeltaTime);
	virtual void ApplyStatus();
	//M�todos para Status que possuem dura��o
	virtual void StatusDuring(float deltaSeconds);
	virtual void RemoveStatus();

	void InitWithCharacter(AWalkCharacter* actor);
	void InitWithAllyTower(AStaticAlly* actor);
};
