// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TowerDefense.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TowerDefense, "TowerDefense" );

DEFINE_LOG_CATEGORY(LogTowerDefense)
 