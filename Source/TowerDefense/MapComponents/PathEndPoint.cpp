// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "PathEndPoint.h"


// Sets default values
APathEndPoint::APathEndPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void APathEndPoint::OnHit(AWalkCharacter* minion)
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		gameState = Cast<AMyGameState>(world->GetGameState());
		if (!minion->GetHeroStatus())
		{
			APawnCharacter* pawnMinion = Cast<APawnCharacter>(minion);
			ARangeCharacter* rangeMinion = Cast<ARangeCharacter>(minion);
			ADefensorCharacter* defensorMinion = Cast<ADefensorCharacter>(minion);
			ASpyCharacter* spyMinion = Cast<ASpyCharacter>(minion);
			if (pawnMinion)
			{
				gameState->updateCountMinionsEnd(ECharactersType::T_PAWN);
			}
			if (rangeMinion)
			{
				gameState->updateCountMinionsEnd(ECharactersType::T_RANGE);
			}
			if (defensorMinion)
			{
				gameState->updateCountMinionsEnd(ECharactersType::T_DEFENSOR);
			}
			if (spyMinion)
			{
				gameState->updateCountMinionsEnd(ECharactersType::T_SPY);
			}
		}
		else
		{
			gameState->updateCountMinionsEnd(ECharactersType::T_HERO);
		}
		minion->Destroy();
	}
}

