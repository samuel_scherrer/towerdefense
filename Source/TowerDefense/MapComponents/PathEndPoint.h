// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Characters/WalkCharacter.h"
#include "../Characters/DefensorCharacter.h"
#include "../Characters/PawnCharacter.h"
#include "../Characters/SpyCharacter.h"
#include "../Characters/RangeCharacter.h"
#include "../GameState/MyGameState.h"
#include "GameFramework/Actor.h"
#include "PathEndPoint.generated.h"

UCLASS()
class TOWERDEFENSE_API APathEndPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APathEndPoint();

protected:
	UFUNCTION(BlueprintCallable, Category = "On Hit")
	void OnHit(AWalkCharacter* minion);
	AMyGameState* gameState;
	
};
