// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "MinionsSpawner.h"


// Sets default values
AMinionsSpawner::AMinionsSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("Spawner"));
	RootComponent = box;

	//Carrega os blueprints de cada tipo de minion
	//**DEFENSOR **//
	static ConstructorHelpers::FObjectFinder<UClass>
		DefensorObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/BP_Defensor.BP_Defensor_C'"));
	if (DefensorObj.Object)
	{
		BP_defensor = DefensorObj.Object;
	}


	//** RANGE **//
	static ConstructorHelpers::FObjectFinder<UClass>
		RangeObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/BP_Range.BP_Range_C'"));
	if (RangeObj.Object)
	{
		BP_range = RangeObj.Object;
	}

	//** Spy **//
	static ConstructorHelpers::FObjectFinder<UClass>
		SpyObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/BP_Spy.BP_Spy_C'"));
	if (SpyObj.Object)
	{
		BP_spy = SpyObj.Object;
	}

	//** Pawn **//
	static ConstructorHelpers::FObjectFinder<UClass>
		PawnObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/BP_Pawn.BP_Pawn_C'"));
	if (PawnObj.Object)
	{
		BP_pawn = PawnObj.Object;
	}

}

// Called when the game starts or when spawned
void AMinionsSpawner::BeginPlay()
{
	Super::BeginPlay();

	if (spline != NULL)
	{
		//Setamos a posi��o do spawner para a mesma posi��o onde o spline come�a
		this->SetActorLocation(spline->GetActorLocation());
	}
	else {
	}
	
}

// Called every frame
void AMinionsSpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AMinionsSpawner::SpawnMinion(ECharactersType type, bool hasHeroStatus)
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		//Parametros para spawn
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;
		AWalkCharacter *minion = NULL;

		//Instancia o personagem dado o parametro recebido
		switch (type)
		{
		case ECharactersType::T_RANGE:
			minion = (AWalkCharacter*)world->SpawnActor<ARangeCharacter>(BP_range,
				this->GetActorLocation(), this->GetActorRotation(), spawnParams);
			if (hasHeroStatus)
			{
				Cast<ARangeCharacter>(minion)->SetHeroStatus();
			}
			break;
		case ECharactersType::T_DEFENSOR:
			minion = (AWalkCharacter*)world->SpawnActor<ADefensorCharacter>(BP_defensor,
				this->GetActorLocation(), this->GetActorRotation(), spawnParams);
			if (hasHeroStatus)
			{
				Cast<ADefensorCharacter>(minion)->SetHeroStatus();
			}
			break;
		case ECharactersType::T_SPY:
			minion = (AWalkCharacter*)world->SpawnActor<ASpyCharacter>(BP_spy,
				this->GetActorLocation(), this->GetActorRotation(), spawnParams);
			if (hasHeroStatus)
			{
				Cast<ASpyCharacter>(minion)->SetHeroStatus();
			}
			break;
		case ECharactersType::T_PAWN:
			minion = (AWalkCharacter*)world->SpawnActor<APawnCharacter>(BP_pawn,
				this->GetActorLocation(), this->GetActorRotation(), spawnParams);
			break;
		default:
			break;
		}


		//Se tem criou a instancia
		if (minion != NULL)
		{
			//Seta o caminho pelo qual o personagem vai se deslocar inicialmente
			minion->SetSplineComponent(spline);

			//Setar varia��o do dado personagem
			float x = FMath::FRandRange(-100, 100);
			float y = FMath::FRandRange(-100, 100);
			minion->SetMovementVariation(FVector2D(x, y));

			//REMOVER APOS AJUSTE DE VALORES
			setChangedValues(type, minion);
		}
	}
}


void AMinionsSpawner::setChangedValues(ECharactersType type, AWalkCharacter* minion) 
{
	switch (type)
	{
	case ECharactersType::T_RANGE:
		if (rangeHasChangedValues)
		{
			ARangeCharacter* range = Cast<ARangeCharacter>(minion);
			range->life = rangeMaxLife;
			range->maxLife = rangeMaxLife;
			range->damage = rangeDamage;
			range->baseSpeed = rangeSpeed;
			range->attackRate = rangeAtkRate;
		}
		break;
	case ECharactersType::T_DEFENSOR:
		if (defensorHasChangedValues)
		{
			ADefensorCharacter* defensor = Cast<ADefensorCharacter>(minion);
			defensor->life = defensorMaxLife;
			defensor->maxLife = defensorMaxLife;
			defensor->workSpeedDrop = defensorSlow;
			defensor->baseSpeed = defensorSpeed;
		}
		break;
	case ECharactersType::T_SPY:
		if (spyHasChangedValues)
		{
			ASpyCharacter* spy = Cast<ASpyCharacter>(minion);
			spy->life = spyMaxLife;
			spy->maxLife = spyMaxLife;
			spy->persuasion = persuation;
			spy->baseSpeed = spySpeed;
		}
		break;
	case ECharactersType::T_PAWN:
		if (pawnHasChangedValues)
		{
			APawnCharacter* pawn = Cast<APawnCharacter>(minion);
			pawn->life = pawnMaxLife;
			pawn->maxLife = pawnMaxLife;
			pawn->baseSpeed = pawnSpeed;
		}
		break;
	case ECharactersType::T_HERO:
		break;
	case ECharactersType::T_ERROR:
		break;
	default:
		break;
	}

}