// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "GameStateVo.h"

UGameStateVo::UGameStateVo()
{
	totalMinionsEnd = 0;
	totalOfPawnsEnd = 0;
	totalOfDefensorsEnd = 0;
	totalOfRangesEnd = 0;
	totalOfSpiesEnd = 0;
	totalOfCharactersWalking = 0;
}

void UGameStateVo::UpdateBackup()
{
	backupTotalOfDefensors = totalOfDefensors;
	backupTotalOfSpies = totalOfSpies;
	backupTotalOfPawns = totalOfPawns;
	backupTotalOfRanges = totalOfRanges;
	backupTotalOfMinionsOnBegin = totalOfMinionsOnBegin;
	backupCurrLevel = currLevel;
	backupCurrChapter = currChapter;
	backupHeroType = heroType;
	backupCanUseHero = canUseHero;
}

void UGameStateVo::LoadBackup()
{
	totalOfMinionsOnBegin = backupTotalOfMinionsOnBegin;
	totalOfDefensors = backupTotalOfDefensors;
	totalOfRanges = backupTotalOfRanges;
	totalOfSpies = backupTotalOfSpies;
	totalOfPawns = backupTotalOfPawns;
	currLevel = backupCurrLevel;
	currChapter = backupCurrChapter;
	heroType = backupHeroType;
	canUseHero = backupCanUseHero;
}

void UGameStateVo::UpdateCountersAfterMatch()
{
	totalOfDefensors = totalOfDefensorsEnd;
	totalOfPawns = totalOfPawnsEnd;
	totalOfSpies = totalOfSpiesEnd;
	totalOfRanges = totalOfRangesEnd;
	totalOfMinionsAvaiableOnMatch = totalMinionsEnd;
	totalOfMinionsOnBegin = totalOfMinionsAvaiableOnMatch;
}

void UGameStateVo::CalculateRating()
{
	float in = (float)totalOfMinionsOnBegin;
	float en = (float)totalMinionsEnd;
	float totalDead = in - en;
	matchRating = ((in - totalDead) / in) * 100;
}

void UGameStateVo::UpdateLevelAndChapter()
{
	//Passa para a pr�xima fase
	currLevel++;
	if (currLevel > 3)
	{
		NextChapter();
	}
}

void UGameStateVo::SetupNewGame()
{
	switch (difficultLevel)
	{
	case 0:
		//Valores do new game. Podemos variar eles de acordo com o nivel de dificuldade por exemplo
		totalOfMinionsOnBegin = 50;
		totalOfDefensors = 5;
		totalOfPawns = 5;
		totalOfSpies = 20;
		totalOfRanges = 20;
		break;
	case 1:
		//Valores do new game. Podemos variar eles de acordo com o nivel de dificuldade por exemplo
		totalOfMinionsOnBegin = 40;
		totalOfDefensors = 0;
		totalOfPawns = 40;
		totalOfSpies = 0;
		totalOfRanges = 0;
		break;
	case 2:
		//Valores do new game. Podemos variar eles de acordo com o nivel de dificuldade por exemplo
		totalOfMinionsOnBegin = 40;
		totalOfDefensors = 0;
		totalOfPawns = 20;
		totalOfSpies = 0;
		totalOfRanges = 20;
		break;
	}
	canUseHero = true;
	heroType = DEFENSOR_TYPE;

	currLevel = 1;
	currChapter = 1;
}

void UGameStateVo::ClearValuesForInitMatch()
{
	totalOfMinionsAvaiableOnMatch = totalOfMinionsOnBegin;
}

void UGameStateVo::NextChapter()
{
	currChapter++;
	switch (currChapter)
	{
	case 2:
		canUseHero = true;
		heroType = RANGE_TYPE;
		break;
	case 3:
		canUseHero = true;
		heroType = SPY_TYPE;
		break;
	case 4:
		canUseHero = false;
		break;
	}
}

bool UGameStateVo::CheckWinCondition()
{
	if (totalOfMinionsAvaiableOnMatch == 0 && totalOfCharactersWalking == 0 && totalMinionsEnd > 0)
		return true;
	else
		return false;
}

bool UGameStateVo::CheckLoseCondition()
{
	if (totalOfMinionsAvaiableOnMatch == 0 && totalOfCharactersWalking == 0 && totalMinionsEnd == 0)
		return true;
	else
		return false;

}

bool UGameStateVo::CheckEndGameCondition()
{
	if (currLevel >= totalOfLevels && currChapter >= totalOfChapters)
		return true;
	else
		return false;
}

void UGameStateVo::updateSpiesCount()
{
	UE_LOG(LogTemp, Warning, TEXT("VO Spawn spies init %d"), totalOfCharactersWalking);
	//Decrementa a quantidade de spies
	totalOfSpies--;
	//Incrementa a quantidade de minions andando
	totalOfCharactersWalking++;
	//Decrementa o total de minions pois ele esta em uso
	totalOfMinionsAvaiableOnMatch--;
	UE_LOG(LogTemp, Warning, TEXT("VO  Spawn spies end %d"), totalOfCharactersWalking);
}

void UGameStateVo::updateDefensorsCount()
{
	//Decrementa a quantidade de defensors
	totalOfDefensors--;
	//Incrementa a quantidade de minions andando
	totalOfCharactersWalking++;
	//Decrementa o total de minions pois ele esta em uso
	totalOfMinionsAvaiableOnMatch--;
}

void UGameStateVo::updatePawnsCount()
{
	//Decrementa a quantidade de pe�es
	totalOfPawns--;
	//Incrementa a quantidade de minions andando
	totalOfCharactersWalking++;
	//Decrementa o total de minions pois ele esta em uso
	totalOfMinionsAvaiableOnMatch--;
}

void UGameStateVo::updateRangesCount()
{
	UE_LOG(LogTemp, Warning, TEXT("VO Spawn range init %d"), totalOfCharactersWalking);
	//Decrementa a quantidade de ranges
	totalOfRanges--;
	//Incrementa a quantidade de minions andando
	totalOfCharactersWalking++;
	//Decrementa o total de minions pois ele esta em uso
	totalOfMinionsAvaiableOnMatch--;
	UE_LOG(LogTemp, Warning, TEXT("VO Spawn range end %d"), totalOfCharactersWalking);
}

bool UGameStateVo::updateCanUseHero()
{
	if (canUseHero)
	{
		UE_LOG(LogTemp, Warning, TEXT("VO Spawn hero %d"), totalOfCharactersWalking);
		canUseHero = false;
		//gameStateVo->totalOfCharactersWalking++;
		//gameStateVo->totalOfMinionsAvaiableOnMatch--;
		return true;
	}
	else
	{
		return false;
	}
}

void UGameStateVo::updateCountMinionsEnd(int32 type)
{
	UE_LOG(LogTemp, Warning, TEXT("VO Count end init %d"), totalOfCharactersWalking);
	if (type == DEFENSOR_TYPE)
	{
		totalOfDefensorsEnd++;
	}
	else if (type == RANGE_TYPE)
	{
		totalOfRangesEnd++;
	}
	else if (type == SPY_TYPE)
	{
		totalOfSpiesEnd++;
	}
	else if (type == PAWN_TYPE)
	{
		totalOfPawnsEnd++;
	}
	else if (type == HERO_TYPE)
	{
		canUseHero = true;
	}

	if (type != HERO_TYPE) {
		//Decrementa a quantidade de minions andando
		totalOfCharactersWalking--;
		//Incrementa o total de minions que chegaram no final
		totalMinionsEnd++;
	}
	UE_LOG(LogTemp, Warning, TEXT("VO Count end end %d"), totalOfCharactersWalking);
}