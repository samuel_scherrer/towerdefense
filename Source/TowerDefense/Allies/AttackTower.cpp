// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "AttackTower.h"

AAttackTower::AAttackTower()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Carrega o blueprint do tipo de projetil usado
	//**SIMPLE PROJECTILE **//
	static ConstructorHelpers::FObjectFinder<UClass>
		ProjectileObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/Objects/BP_SimpleProjectile.BP_SimpleProjectile_C'"));
	if (ProjectileObj.Object)
	{
		BP_SimpleProjectile = (UClass*)ProjectileObj.Object;
	}

	//Propriedades da torre
	life = 200;
	attackRate = 2.5;
	accumulatedTime = 0;
	damage = 25;
}



void AAttackTower::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Verifica se tem algum alvo. Se n�o tem escolhe um
	if (currTarget == NULL)
	{
		currTarget = SelectTarget();
	}
	//Se j� tem um alvo ent�o incrementa o tempo acumulado
	else
	{
		accumulatedTime += DeltaSeconds;
	}

	//Se j� passou o tempo attackRate desde o �ltimo attack e tem algum algo ent�o ataca
	if (accumulatedTime > 1 / attackRate && currTarget != NULL)
	{
		accumulatedTime = 0;
		Attack();
	}
}

void AAttackTower::OnEnterAreaOfAttack(AStaticEnemy * target)
{
	listOfEnemies.Add(target);
}

void AAttackTower::OnExitAreaOfAttack(AStaticEnemy * target)
{
	int itemPos;
	if (listOfEnemies.Find(target, itemPos))
	{
		listOfEnemies.RemoveAt(itemPos, 1, true);
		//Se o alvo saiu da area de ataque ent�o perdeu o currTarget
		if (currTarget == target) {
			//Informa que n�o � mais um alvo
			currTarget = NULL;
		}
	}
}

void AAttackTower::SetTowerMeshExtent(FVector BoxExtent)
{
	towerMeshBoxExtent = BoxExtent;
}

void AAttackTower::SetTowerMesh(UStaticMeshComponent * mesh)
{
	towerMesh = mesh;
}

FVector const AAttackTower::GetTowerMeshExtent()
{
	return towerMeshBoxExtent;
}

AStaticEnemy * AAttackTower::SelectTarget()
{
	if (listOfEnemies.Num() > 0)
	{
		//Por hora retorna o primeiro
		AStaticEnemy* target = listOfEnemies.HeapTop();
		if (target)
		{
			return target;
		}
	}
	return nullptr;
}

void AAttackTower::Attack()
{
	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		//Parametros para spawn
		FActorSpawnParameters spawnParams;
		spawnParams.Owner = this;
		spawnParams.Instigator = Instigator;

		//O projetil deve ser criado no topo da torre.
		FVector towerLoc = GetActorLocation();
		float projectileZLoc = towerLoc.Z + towerMeshBoxExtent.Z;
		//Tiramos um quinto do tamanho da torre para garantir que fica dentro da torre
		projectileZLoc = projectileZLoc - towerMeshBoxExtent.Z / 5;

		FVector projectileLoc = FVector(towerLoc.X, towerLoc.Y, projectileZLoc);


		ASimpleProjectile* const projectile = world->SpawnActor<ASimpleProjectile>(BP_SimpleProjectile,
			projectileLoc, this->GetActorRotation(), spawnParams);
		//Seta o alvo para o projetil
		projectile->Start(currTarget, damage);

	}
}
