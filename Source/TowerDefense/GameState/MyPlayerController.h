// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "../HUD/GameHUD.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMyPlayerController();

protected:
	uint32 bMouseBtnClicked : 1;
	uint32 bIsFirstTouch : 1;
	uint32 bIsSecondTouch : 1;

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	/** Input handlers for set clicked actions */
	void OnSetActionPressed();
	void OnSetActionReleased();

	void OnSetActionTouched(const FVector Location);


	/** Call action if mouse clicked */
	void CallAction();

	// Input handlers
	void OnMoveForwardAxis(float axisValue);
	void OnMoveRightAxis(float axisValue);
	void OnZoomInAction();
	void OnZoomOutAction();	


	//Variables for touch movements
	FVector2D firstTouchInitLoc;
	FVector2D secondTouchInitLoc;
	float swipeThreshold = 1;
	float swipeSpeed = 2000;

	//Listeners for touch event
	void OnTouchAction(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchRelease(const ETouchIndex::Type FingerIndex, const FVector Location);

	void CheckForTouchMoves();
	void CheckForPinchDistance();

private:
	AGameHUD* myHUD;

	AGameHUD* GetMyHUD();
};
