// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "GameHUD.h"

AGameHUD::AGameHUD()
{

}

void AGameHUD::SetAlliesSpawner(AAlliesSpawner* spawner)
{
	selectedAlliesSpawner = spawner;
	//Informa a HUD que deve mostrar o seletor de torres aliadas
	FString command = FString::Printf(TEXT("ShowAllySelectionWidget"));
	FOutputDeviceDebug debug;
	this->CallFunctionByNameWithArguments(*command, debug, NULL, true);
}

void AGameHUD::SetQuarterTower(AQuarterTower * tower)
{
	quarterTower = tower;
	//Informa a HUD que deve mostrar o seletor de minions do quartel
	FString command = FString::Printf(TEXT("ShowQuarterSpawnWidget"));
	FOutputDeviceDebug debug;
	this->CallFunctionByNameWithArguments(*command, debug, NULL, true);
}


