// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Characters/WalkCharacter.h"
#include "SpyCharacter.generated.h"

class AStaticEnemy;
/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASpyCharacter : public AWalkCharacter
{
	GENERATED_BODY()
	
public:
	// Sets default values for this character's properties
	ASpyCharacter();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	//Indica quanta for�a o spy tem para dominar uma torre
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionProperties")
	float persuasion;

	//Indica que a torre selecional tal spy para ser destruido
	bool isMarkedToDestroy;

	//Informa o target do spy
	void SetCurrTarget(AStaticEnemy* target) { currTarget = target; }

	//Consede ao character status de hero 
	virtual void SetHeroStatus() override;

protected:
	UFUNCTION(BlueprintCallable, Category = "Selecting Target")
		void OnEnterInRange(AStaticEnemy* target);

	UFUNCTION(BlueprintCallable, Category = "Selecting Target")
		void OnExitRange(AStaticEnemy* target);

	void ExecCharacterState(float DeltaTime);

private:
	//Posicao onde est� a torre sendo atacada
	FVector towerLoc;

	AStaticEnemy* currTarget;

	//Collecao de alvos
	TArray<AStaticEnemy*> targetsCollection;

	//** Como clicamos na torre nao precisamos mais que o spy olhe por uma torre. Ao se clicar na torre a torre notifica o spy
	//Verifica entre as torres que est�o sob alvo deste minion
	//Se algum est� com a flag isSelectedTargetForSpy em true
	//AStaticEnemy* CheckForAttackCommand();

	void Spy();
	void SpawnAllyTowerSelector(FVector location);

	UClass* BP_AllyTowerSpawner;
};
