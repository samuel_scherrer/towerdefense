// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "../Allies/QuarterTower.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "Blueprint/UserWidget.h"
#include "WalkCharacter.h"

// Sets default values
AWalkCharacter::AWalkCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Inicia a distancia andada em zero
	walkedDistance = 0;

	//Default statement
	characterState = ECharacterStates::S_WALKING;

	//Segue exatamente o pathpoint passado
	movementVariation = FVector2D(0, 0);

	//Carrega o blueprint da widget de vida
	//**Health Widget **//
	static ConstructorHelpers::FObjectFinder<UClass>
		HealthWidgetObj(TEXT("Class'/Game/TopDownCPP/Blueprints/Characters/HUD/BP_HealthBarWidget.BP_HealthBarWidget_C'"));
	if (HealthWidgetObj.Object)
	{
		BP_HealthWidget = HealthWidgetObj.Object;
	}

	healthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthWidget"));
	healthWidget->SetRelativeLocation(FVector(0, 0, 130));
	healthWidget->SetWidgetClass(BP_HealthWidget);
	healthWidget->SetupAttachment(RootComponent);

	//Ninguem � hero no come�o
	hasHeroStatus = false;
	wasDestroyCalled = false;

}

// Called when the game starts or when spawned
void AWalkCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Verifica se tem um mundo valido
	UWorld* const world = GetWorld();
	if (world)
	{
		gameState = Cast<AMyGameState>(world->GetGameState());
	}
}

// Called every frame
void AWalkCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (!wasDestroyCalled) 
	{

		if (lifeChanging) 
		{
			//Informa o widget da vida
			FString command = FString::Printf(TEXT("Update %f"), life / maxLife);
			FOutputDeviceDebug debug;
			healthWidget->GetUserWidgetObject()->CallFunctionByNameWithArguments(*command, debug, NULL, true);
		}

		if (life <= 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("life < 0"));
			characterState = ECharacterStates::S_DEAD;
		}

		ExecCharacterState(DeltaTime);
	}
}

void AWalkCharacter::ExecCharacterState(float DeltaTime)
{
	switch (characterState)
	{
	case ECharacterStates::S_WALKING:
		Move(DeltaTime);
		break;
	case ECharacterStates::S_IDLE:
		break;
	case ECharacterStates::S_DEAD:
		UE_LOG(LogTemp, Warning, TEXT("Call of death"));
		wasDestroyCalled = true;
		gameState->notifyMinionDeath();
		this->Destroy();
		break;
	case ECharacterStates::S_GONE:
		wasDestroyCalled = true;
		this->Destroy();
		break;
	case ECharacterStates::S_MOVETO:
		MoveTo(DeltaTime);
		break;
	case ECharacterStates::S_HIDDEN:
		if (!bHidden)
		{
			SetActorHiddenInGame(true);
			characterState = ECharacterStates::S_IDLE;
		}
		break;
	default:
		break;
	}
}

FVector AWalkCharacter::GetPathLocationWithDistance(float distance)
{
	return pathSplineComponent->pathSpline->GetWorldLocationAtDistanceAlongSpline(distance);
}

// Called to bind functionality to input
void AWalkCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AWalkCharacter::Move(float deltaTime)
{
	if (pathSplineComponent != NULL) {
		//Obtemos o componente spline que o personagem est� seguindo
		USplineComponent const *spline = pathSplineComponent->pathSpline;

		//Obtemos a localiza��o e a rota��o no ponto em que o personagem est�
		//para isso usamos o quanto o personagem j� andou
		FVector locAtDistance = spline->GetWorldLocationAtDistanceAlongSpline(walkedDistance);
		FRotator rotAtDistance = spline->GetWorldRotationAtDistanceAlongSpline(walkedDistance);

		//A nova posi��o levar� em conta o x e y do spline e o z do personagem
		FVector newLocation = FVector(locAtDistance.X + movementVariation.X,
				locAtDistance.Y + movementVariation.Y, GetActorLocation().Z);

		FRotator currRotation = GetActorRotation();
		FRotator newRotation = FRotator(currRotation.Pitch, rotAtDistance.Yaw, currRotation.Roll);

		walkSpeed = baseSpeed * deltaTime;

		//Se gerou uma nova localiza��o. Ent�o move
		if (newLocation != this->GetActorLocation())
		{
			//Atualiza o quanto andou
			walkedDistance += walkSpeed;

			SetActorRelativeLocation(newLocation);
			SetActorRelativeRotation(newRotation);
		}

	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Setar o PathPoint que o personagem deve seguir!"));
	}
}

void AWalkCharacter::QuarterNewStatus(bool newStatus, AQuarterTower* theQuarter)
{
	//Se estava andando e agora o status da torre � fechado � pq antes estava em aberto e logo
	//o minion estava indo para a torre. Mandar ele de volta para o path
	if (characterState == ECharacterStates::S_MOVETO && !newStatus)
	{
		//Pegamos a localizacao onde o personagem estava antes de iniciar o moveTo
		FVector prevLoc = pathSplineComponent->pathSpline->GetWorldDirectionAtDistanceAlongSpline(walkedDistance);
		float distance = (FVector2D(GetActorLocation().X, GetActorLocation().Y) - FVector2D(prevLoc.X, prevLoc.Y)).Size();
		walkedDistance = walkedDistance + distance;
		FVector newLoc = pathSplineComponent->pathSpline->GetWorldDirectionAtDistanceAlongSpline(walkedDistance);
		SetMoveToLocation(newLoc, ECharacterStates::S_WALKING);
	}
}


void AWalkCharacter::SetMoveToLocation(FVector location, ECharacterStates goToState)
{
	moveToDirection = location - GetActorLocation();
	moveToDirection.Normalize();

	//Muda o personagem de estado
	characterState = ECharacterStates::S_MOVETO;

	//Guarada infos
	stateAfterMoveTo = goToState;
	moveToLoc = location;

	//Gira o minion para a dire��o. S� gira ele no eixo Z
	SetActorRelativeRotation(FRotator(0, moveToDirection.Rotation().Yaw, 0));
}

void AWalkCharacter::MoveTo(float deltaTime)
{
	//calcula a distancia at� o ponto esperado
	float distance = (FVector2D(GetActorLocation().X, GetActorLocation().Y) - FVector2D(moveToLoc.X, moveToLoc.Y)).Size();
	if (distance > (deltaTime * baseSpeed))
	{
		FVector myLoc = this->GetActorLocation();
		myLoc += moveToDirection * deltaTime * baseSpeed;

		this->SetActorRelativeLocation(FVector(myLoc.X, myLoc.Y, GetActorLocation().Z));
	}
	else {
		this->SetActorRelativeLocation(FVector(moveToLoc.X, moveToLoc.Y, GetActorLocation().Z));
		characterState = stateAfterMoveTo;
		//Chama o callback
		OnMoveToFinish.Broadcast(this);
	}
}

float AWalkCharacter::GetWalkSpeed()
{
	return walkSpeed;
}

void AWalkCharacter::SetWalkSpeed(float newSpeed) 
{
	baseSpeed = newSpeed;
}

void AWalkCharacter::SetCharacterState(ECharacterStates state)
{
	characterState = state;
}

void AWalkCharacter::SetSplineComponent(APathPoint* newPath)
{
	pathSplineComponent = newPath;
}

void AWalkCharacter::SetMovementVariation(FVector2D movVartion)
{
	movementVariation = FVector2D(movVartion.X, movVartion.Y);
}

void AWalkCharacter::NotifyIsUnderAttack(bool isUnderAttack)
{
	lifeChanging = isUnderAttack;
}

FVector2D AWalkCharacter::GetMovementVariation()
{
	return movementVariation;
}

void AWalkCharacter::SetHeroStatus()
{
	hasHeroStatus = true;

	//Editar as propriedades do character em cada um
}