// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "Objects/Projectile.h"
#include "ShooterTower.h"

AShooterTower::AShooterTower()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Propriedades da torre
	life = 200;
	maxLife = life;
	attackRate = 2.5;
	accumulatedTime = 0;
	loyalty = 500;
	maxLoyalty = loyalty;
	damage = 40;
}

// Called every frame
void AShooterTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Verifica se tem algum defensor dentro do range e se tiver troca de tarde
	CheckForDefensorCharacter();

	//Verifica se tem algum alvo. Se n�o tem escolhe um
	if (currTarget == NULL)
	{
		currTarget = SelectTarget();
		//Se achou alvo informa que ele est� sob ataque
		if (currTarget != NULL)
		{
			AWalkCharacter* character = Cast<AWalkCharacter>(currTarget);
			if (character)
			{
				character->NotifyIsUnderAttack(true);
			}
		}
	}
	//Se j� tem um alvo ent�o incrementa o tempo acumulado
	else
	{
		accumulatedTime += DeltaTime;
	}

	//Se o alvo ficou hidden para de atirar nele e pega outro target
	if (currTarget != NULL)
	{
		if (currTarget->bHidden)
		{
			//Se o alvo atual � do tipo awalkcharacter notifica que n�o est� mais sob atk
			AWalkCharacter* character = Cast<AWalkCharacter>(currTarget);
			if (character)
			{
				character->NotifyIsUnderAttack(false);
			}
			currTarget = NULL;
			accumulatedTime = 0;
		}
	}

	//Se j� passou o tempo attackRate desde o �ltimo attack e tem algum algo ent�o ataca
	if (accumulatedTime > 1/attackRate && currTarget != NULL)
	{
		accumulatedTime = 0;
		Attack();
	}
}

void AShooterTower::CheckForDefensorCharacter()
{
	//Verifica se o target atual existe e se � do tipo DefensorCharacter
	if (currTarget != NULL)
	{
		//Avalia o target � do tipo defensorCharacter
		ADefensorCharacter* defensor = Cast<ADefensorCharacter>(currTarget);
		//Se o alvo atual n�o � defensor procura pro um defensor
		if (!defensor)
		{
			//Temo defenso na lista?
			if (defensorCollection.Num() > 0)
			{
				//Procura pelo defensor que n�o esteja invisivel
				for (ADefensorCharacter* tanker : defensorCollection)
				{
					if (!tanker->bHidden)
					{
						//Se tem defensor dentro do range ent�o troca de targe pois o atual n�o � defensor
						AWalkCharacter* character = Cast<AWalkCharacter>(currTarget);
						if (character)
						{
							//Se for do tipo AWalkCharacter ent�o informa o status do atk
							character->NotifyIsUnderAttack(false);
						}
						//Seta o novo alvo
						currTarget = Cast<AActor>(tanker);
						//Informa q ele est� sob atk
						tanker->NotifyIsUnderAttack(true);
						//Para pois ja achou um
						break;
					}
				}
			}
		}
	}
}

AActor* AShooterTower::SelectTarget()
{
	//Se tem algum defensor dentor da area de ataque vamos escolher ele

	//Por agora pega o primeiro que n�o esteja invisivel
	if (defensorCollection.Num() > 0) {
		for (ADefensorCharacter* tanker : defensorCollection)
		{
			if (!tanker->bHidden)
			{
				return Cast<AActor>(tanker);
			}
		}
	}
	if(targetsCollection.Num() > 0)
	{
		for (AWalkCharacter* character : targetsCollection)
		{
			if (!character->bHidden)
			{
				return Cast<AActor>(character);
			}
		}
	}
	if (alliesCollection.Num() > 0)
	{
		for (AStaticAlly* allyTower : alliesCollection)
		{
			return Cast<AActor>(allyTower);
		}
	}

	//Se nao retornou nada at� aqui ent�o
	return nullptr;
}

void AShooterTower::Attack()
{
	//Verifica se foi setado um tipo de projetil para a torre
	if(projectileToSpawn != NULL)
	{
		//Verifica se tem um mundo valido
		UWorld* const world = GetWorld();
		if (world)
		{
			//Parametros para spawn
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = Instigator;
			
			//O projetil deve ser criado no topo da torre.
			FVector towerLoc = GetActorLocation();
			float projectileZLoc = towerLoc.Z + towerMeshBoxExtent.Z;
			//Tiramos um quinto do tamanho da torre para garantir que fica dentro da torre
			projectileZLoc = projectileZLoc - towerMeshBoxExtent.Z / 5;

			FVector projectileLoc = FVector(towerLoc.X, towerLoc.Y, projectileZLoc);


			AProjectile* const projectile = world->SpawnActor<AProjectile>(projectileToSpawn, 
								projectileLoc, this->GetActorRotation(), spawnParams);
			//Seta o alvo para o projetil
			projectile->Start(currTarget, damage);
		}

	}

}


