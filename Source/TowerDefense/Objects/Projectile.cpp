// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "../Enemies/StaticEnemy.h" 
#include "Projectile.h"


// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Constantes
	velocity = 5000;
	damage = 40;

	//Estado inicial
	projectileState = EProjectileStates::S_MOVING;
}

void AProjectile::Start(AActor* target, float dmg)
{
	damage = dmg;
	AWalkCharacter* character = Cast<AWalkCharacter>(target);
	AStaticAlly* allyTower = Cast<AStaticAlly>(target);
	if (character)
	{
		InitWithCharacter(character);
	}
	else if (allyTower)
	{
		InitWithAllyTower(allyTower);
	}
}

void AProjectile::InitWithCharacter(AWalkCharacter* actor)
{
	target = actor;

	FVector actorLocation = actor->GetActorLocation();
	FVector myLocation = this->GetActorLocation();
	//Se o minion est� andando na pathspline
	if (actor->GetCharacterState() == ECharacterStates::S_WALKING)
	{
		FVector distanceVector = myLocation - actorLocation;
		float t0 = distanceVector.Size() / velocity;
		float actorSpeed = actor->baseSpeed;
		float deltaDistance = actorSpeed * t0;
		USplineComponent* pathSpline = actor->pathSplineComponent->pathSpline;
		FVector predictedLocation = pathSpline->GetWorldLocationAtDistanceAlongSpline(actor->walkedDistance + deltaDistance);
		FVector2D targetMovVariation = actor->GetMovementVariation();
		//Ajustamos a componente z para que o projetil acerte na parte superior do actor
		float collisionRadius;
		float halfHeight;
		actor->GetCapsuleComponent()->CalcBoundingCylinder(collisionRadius, halfHeight);
		//O projetil deve se dirigir para os membros superiores do alvo. Tiramos um para 
		//garantir que n�o passe sobre a cabe�a
		float targetHeightCorrection = predictedLocation.Z + halfHeight;
		predictedLocation.Set(predictedLocation.X + targetMovVariation.X,
			predictedLocation.Y + targetMovVariation.Y, targetHeightCorrection);

		//Calculamos finalmente a dire��o do movimento
		direction = predictedLocation - myLocation;
	}
	//Se o minion est� andando pra dentro de um quartel
	else
	{
		direction = actorLocation - myLocation;
	}
	direction.Normalize();
}

void AProjectile::InitWithAllyTower(AStaticAlly * actor)
{
	target = actor;
	FVector actorLocation = actor->GetActorLocation();
	FVector myLocation = this->GetActorLocation();

	//Calculamos finalmente a dire��o do movimento
	direction = actorLocation - myLocation;
	direction.Normalize();
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	switch (projectileState)
	{
	case EProjectileStates::S_MOVING:
		Move(DeltaTime);
		break;
	case EProjectileStates::S_APPLY:
		mesh->SetHiddenInGame(true, true);
		ApplyStatus();
		break;
	case EProjectileStates::S_DURING:
		StatusDuring(DeltaTime);
		break;
	case EProjectileStates::S_FINALIZE:
		RemoveStatus();
		break;
	case EProjectileStates::S_DONE:
		this->Destroy();
		break;
	default:
		break;
	}
}

void AProjectile::Move(float DeltaTime)
{
	FVector myLoc = this->GetActorLocation();
	myLoc += direction * DeltaTime * velocity;

	this->SetActorRelativeLocation(myLoc);
}

void AProjectile::OnHit(AActor* reachedTarget)
{
	//Verifica se colidiu com um personagem
	AWalkCharacter* targetCharacter = Cast<AWalkCharacter>(reachedTarget);
	if (Cast<AWalkCharacter>(reachedTarget) || Cast<AStaticAlly>(reachedTarget))
	{
		if (reachedTarget == target)
		{
			//Se sim ent�o vai para o estado apply
			projectileState = EProjectileStates::S_APPLY;
		}
	}
	else if (Cast<AStaticEnemy>(reachedTarget) == NULL)
	{
		//Se bateu em algo que n�o � minion e nem torre inimiga ent�o destroi
		projectileState = EProjectileStates::S_DONE;
	}
}

void AProjectile::ApplyStatus()
{
	AWalkCharacter* character = Cast<AWalkCharacter>(target);
	AStaticAlly* allyTower = Cast<AStaticAlly>(target);
	if (character)
	{
		//Projetil de status instantaneo
		character->life = character->life - damage;
	}
	else if (allyTower)
	{
		//Projetil de status instantaneo
		allyTower->life = allyTower->life - damage;
	}
	//Marca para destruir
	projectileState = EProjectileStates::S_DONE;

}

void AProjectile::StatusDuring(float deltaSeconds)
{
}

void AProjectile::RemoveStatus() 
{
}

void AProjectile::SetStaticMeshComponent(UStaticMeshComponent* staticMesh)
{
	mesh = staticMesh;
}