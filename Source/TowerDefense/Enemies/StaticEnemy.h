// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Characters/WalkCharacter.h"
#include "../Characters/DefensorCharacter.h"
#include "../Characters/SpyCharacter.h"
#include "../Allies/StaticAlly.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/Pawn.h"
#include "StaticEnemy.generated.h"

UCLASS()
class TOWERDEFENSE_API AStaticEnemy : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float life;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float maxLife;
	//Indica o quanto � necess�rio para dominar uma torre
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float loyalty;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float maxLoyalty;
	//Definido em tiros/segundos
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyProperties")
	float attackRate;

	// Sets default values for this pawn's properties
	AStaticEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorBeginOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnCharacterEnterAreaOfAttack(AWalkCharacter* target);

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorEndOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnCharacterExitAreaOfAttack(AWalkCharacter* target);

	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnAllyExitAreaOfAttack(AStaticAlly* target);

	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnAllyEnterAreaOfAttack(AStaticAlly* target);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SetTowerMeshExtent(FVector BoxExtent);

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SetTowerMesh(UStaticMeshComponent* mesh);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Life")
		UWidgetComponent* healthWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loyalty")
		UWidgetComponent* loyaltyWidget;

	FVector const GetTowerMeshExtent();

	//Collecao de alvos
	TArray<AWalkCharacter*> targetsCollection;
	//Collecao de defensors
	TArray<ADefensorCharacter*> defensorCollection;
	//Collecao de torres alidas
	TArray<AStaticAlly*> alliesCollection;

	//indica que essa torre est� marcada para ser destruida
	bool isMarkedForDestroy;

	void AddOnSpiesList(ASpyCharacter* spy);
	void RemoveFromSpiesList(ASpyCharacter* spy);
	//Esse m�todo s� deve guardar um spy, o primeiro spy a notificar que essa
	//torre deve ser destruida
	void MarkForDestroy(ASpyCharacter* spy);

	//Informa que est� sendo clicada e busca por um spy que tenha range para atk
	//e coloca o spy para o state de atacando
	void NotifySelectedTower();

protected:
	AActor* currTarget;
	//Armazena o tempo passado entre ticks
	float accumulatedTime;

	//Indica que est� sob o range de pelo menos um spy
	UPROPERTY(BlueprintReadOnly, Category = "Tower Status")
	bool isUnderSpyRange;

	TArray<ASpyCharacter*> listOfSpies;

	//Informacoes da mesh
	FVector towerMeshBoxExtent;
	UStaticMeshComponent* towerMesh;

	//O resultado dessa opera��o ir� ser armazenado no TArray defensorCollection
	void CheckCollectionForDefensor();
	//Implementar nos filhos dessa classe.
	virtual AActor* SelectTarget();
	//Implementar nos filhos dessa classe.
	virtual void Attack();

	void NotifySpiesOfDestruction();
	void NotifyOfDestruction();

	UClass* BP_HealthWidget;
	UClass* BP_LoyaltyWidget;
};
