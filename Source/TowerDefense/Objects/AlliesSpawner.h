// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Allies/StaticAlly.h"
#include "AlliesSpawner.generated.h"

UENUM(BlueprintType)
enum class EAlliesType : uint8
{
	A_HEALER		UMETA(DisplayName = "A_Healer"),
	A_DAMAGER		UMETA(DisplayName = "A_Damager"),
	A_QUARTER		UMETA(DisplayName = "A_Quarter")
};

UCLASS()
class TOWERDEFENSE_API AAlliesSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAlliesSpawner();

	//**REMOVER APOS AJUSTE DOS VALORES
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		bool healingTowerChangedValues;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		bool attackTowerChangedValues;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		bool quarterTowerChangedValues;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float healAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float healrate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float healLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float attackDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float attackRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float attackLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugProperties")
		float quarterLife;

	//M�todo chamado para instanciar um objecto da classe passada
	UFUNCTION(BlueprintCallable, Category = "Spawn")
		void SpawnAlly(EAlliesType type);


private:
	UClass* BP_HealingTower;
	UClass* BP_DamagerTower;
	UClass* BP_QuarterTower;
	
	//REMOVER APOS TESTES
	void setChangedValues(EAlliesType type, AStaticAlly* minion);
};
