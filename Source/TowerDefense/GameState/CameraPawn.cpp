// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "CameraPawn.h"


// Sets default values
ACameraPawn::ACameraPawn()
{
	//Habilita a rota��o no eixo Z
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;
	
	bAddDefaultMovementBindings = false;

	//Cria a camera boom
	cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	cameraBoom->SetupAttachment(RootComponent);
	cameraBoom->bAbsoluteRotation = false; //permite o bra�o girar
	cameraBoom->TargetArmLength = 3500.f;
	cameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	cameraBoom->bDoCollisionTest = false; //Sem colisoes para a camera

	// Move camera boom with character only on yaw rotation
	cameraBoom->bUsePawnControlRotation = false;
	cameraBoom->bInheritPitch = false;
	cameraBoom->bInheritRoll = false;
	cameraBoom->bInheritYaw = true;

	// Enables camera lag - matter of taste
	cameraBoom->bEnableCameraLag = true;
	cameraBoom->bEnableCameraRotationLag = true;

	GetCollisionComponent()->bGenerateOverlapEvents = false;
	GetCollisionComponent()->SetCollisionProfileName("NoCollision");

	// Create a camera...
	topDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	topDownCameraComponent->SetupAttachment(cameraBoom, USpringArmComponent::SocketName);
	topDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

void ACameraPawn::ChangeCameraArmLength(float changeValue)
{
	float newValue = cameraBoom->TargetArmLength + (changeValue * 100);
	if (newValue > 100 && newValue < 4000) {
		cameraBoom->TargetArmLength += changeValue * 100.0f;
	}
}

void ACameraPawn::RotateCameraArm(FRotator rotation)
{
	cameraBoom->AddRelativeRotation(rotation);
}

void ACameraPawn::MoveCharacterForward(float changeValue)
{
	AddMovementInput(GetActorForwardVector(), changeValue);
}

void ACameraPawn::MoveCharacterRight(float changeValue)
{
	AddMovementInput(GetActorRightVector(), changeValue);
}

void ACameraPawn::MoveToLoc(FVector location)
{
	this->SetActorLocation(location);
}



