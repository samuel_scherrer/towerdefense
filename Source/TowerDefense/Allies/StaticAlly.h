// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"
#include "StaticAlly.generated.h"

UCLASS()
class TOWERDEFENSE_API AStaticAlly : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AllyProperties")
	float life;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AllyProperties")
	float maxLife;
	// Sets default values for this actor's properties
	AStaticAlly();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorBeginOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnEnterAreaOfAction(AWalkCharacter* target);

	//Atualiza a lista de targets. Deve ser chamado no blueprint pelo evento
	//ActorEndOverlap
	UFUNCTION(BlueprintCallable, Category = "Selecting Targets")
		virtual void OnExitAreaOfAction(AWalkCharacter* target);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Life")
		UWidgetComponent* healthWidget;

protected:
	TArray<AWalkCharacter*> listOfCharacters;

	//Implementar nos filhos dessa classe.
	virtual AActor* SelectTarget();
	
	//Implementar nos filhos dessa classe.
	virtual void Action();

	UClass* BP_HealthWidget;
};
