// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerDefense.h"
#include "SlowTower.h"

ASlowTower::ASlowTower()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	life = 60;
	maxLife = life;
	loyalty = 700;
	maxLoyalty = loyalty;
	slowDebuff = 100;
}

// Called every frame
void ASlowTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (life <= 0)
	{
		RemoveEffect();
	}
}

void ASlowTower::OnCharacterEnterAreaOfAttack(AWalkCharacter* target)
{
	//Chamamos o super para manter a lista de targets atualizada
	Super::OnCharacterEnterAreaOfAttack(target);
	target->baseSpeed = target->baseSpeed - slowDebuff;
}

void ASlowTower::OnCharacterExitAreaOfAttack(AWalkCharacter* target)
{
	//Chamamos o super para manter a lista de targets atualizada
	Super::OnCharacterExitAreaOfAttack(target);
	target->baseSpeed = target->baseSpeed + slowDebuff;
}

void ASlowTower::RemoveEffect()
{
	//Remove o debuff de todos antes de sumir
	for (AWalkCharacter *character : targetsCollection)
	{
		character->baseSpeed = character->baseSpeed + slowDebuff;
	}
}
